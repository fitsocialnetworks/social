package SocNetSim2017;
/*
 * Social_Network.java
 *
 * Created on 22 March 2006
 */

/**
 *
 * @author  Alex Tee Neng Heng
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.*;
import SocNetSim2017.Utility.*;


public class SocialNetwork extends Network {
	// public static final int DISTANCE = 1;
	public static final int DISTANCE_SIMILARITY = 0;
	public static final int DISTANCE = 1;
	public static final int SIMILARITY = 2;
	// public static final 
	
	private Dijkstra distance;
	private int[][] binary_matrix;
	private double[][] similarity_matrix;
	private int[][] distance_matrix;
	private int interaction_type;
	private int interaction_radius = 10;
	private ArrayList<Agent> interact_agents;
	private ArrayList<Vertex> unvisited_vertices;
	private ArrayList<Edge> keys;
	private Hashtable<Integer, Integer> remaining_vertices;
	
	private double influence_max;
	private double influence_min;
	
	private int current_timesteps;
	public double clustering_coefficient;
	public double connectivity;
	public double modularity;
	public double average_path_length;
	public double power_law_coefficient;
	public double correlation_coefficient;
	
	private boolean measure_connectivity;
	private boolean measure_CC;
	private boolean measure_modularity;
	private boolean measure_apl;
	private boolean measure_plawC;
	private boolean measure_correlationC;
	
	private boolean allow_influence;
	private boolean allow_weight;
	private boolean allow_global_interaction;
	
	private int global_interaction_freq;

	public SocialNetwork(int size) {
		super(size);
		this.distance = new Dijkstra();
		this.interact_agents = new ArrayList<Agent>();
		this.unvisited_vertices = new ArrayList<Vertex>();
		this.remaining_vertices = new Hashtable<Integer, Integer>();
		this.keys = new ArrayList<Edge>();
	}
	
	public void init(int size, double conn, int k, double s_prob, int num_l, String newPajekFilename) {
        // BUGFIX:
        // INITIALIZER IN SUPERCLASS SocNetSim2017.Network.java MODIFIED TO HAVE 6 PARAMS - this initializer must adhere as well.
        // -> superclass will in turn calls SocNetSim2017.Graph.java (Super-super-class) init which clears V,E and inits the matrix

        // therefore instead of having only SocNetSim2017.Graph.java (Super-super-class) handle the init... and manually set* everything
        // why not chain: SUPERCLASS SocNetSim2017.Network.java handle the setters, and finally SocNetSim2017.Graph.java for basic init?
	    super.init(size, conn, k, s_prob, num_l, newPajekFilename );
		this.current_timesteps = 0;
		//this.set_connectivity( conn );
		//this.set_k( k );
		//this.set_shortcuts_probability( s_prob );
		//this.set_num_leaf( num_l );
		
		this.binary_matrix = new int[size][size];
		this.similarity_matrix = new double[size][size];
		this.distance_matrix = new int[size][size];
	}
	
	public void initMeasurements(boolean c, boolean CC, boolean m, boolean apl, boolean plawC, boolean correlationC) {
		this.measure_connectivity = c;
		this.measure_CC = CC;
		this.measure_modularity = m;
		this.measure_apl = apl;
		this.measure_plawC = plawC;
		this.measure_correlationC = correlationC;
	}
	
	public int getCurrentTimesteps() {
		return this.current_timesteps;
	}
	
	public void setGlobalInteractionFreq(int freq) {
		this.global_interaction_freq = freq;
	}
	
	public void setAllowGlobalInteraction(boolean allow) {
		this.allow_global_interaction = allow;
	}
	
	public void setAllowInfluence(boolean allow) {
		this.allow_influence = allow;
	}
	
	public boolean getAllowInfluence() {
		return this.allow_influence;
	}
	
	public void setAllowWeight(boolean allow) {
		this.allow_weight = allow;
	}
	
	public boolean getAllowWeight() {
		return this.allow_weight;
	}
	
	public void setInteractionType(int type) {
		this.interaction_type = type;
	}
	
	public void setInteractionRadius(int radius) {
		if ( radius >= 1) {
			this.interaction_radius = radius;
		} else {
			this.interaction_radius = 1;
		}
	}
	
	public void setInfluenceMax(double influence) {
        if ( influence >=0 && influence <= 1.0 ) {
            this.influence_max = influence;
        } else {
            this.influence_max = 0.5;
        }
    }
    
    public double getInfluenceMax() {
        return this.influence_max;
    }

	public void setInfluenceMin(double influence) {
        if ( influence >=0 && influence <= 1.0 ) {
            this.influence_min = influence;
        } else {
            	this.influence_min = 0.5;
        }
    }
    
    public double getInfluenceMin() {
        return this.influence_min;
    }
    
    public void assignInfluenceValue() {
		int min = Integer.MAX_VALUE,
			max = Integer.MAX_VALUE;
		for ( int i = 0; i < super.size(); i++ ) {
		    // BUGFIX: implemented error checking if non-null!!
            System.out.println(super.get_vertex(i).toString());
		    if (super.get_vertex(i).neighbours != null) {
                min = Math.min(min, super.get_vertex(i).neighbours.size());
                max = Math.max(max, super.get_vertex(i).neighbours.size());
            }
		}

		double k = ( this.influence_max - this.influence_min ) / ( Math.log( max - min ) );

		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent)super.get_vertex(i);
			double p = k * Math.log( a.neighbours.size() ) + this.getInfluenceMin();
			a.set_influence(p);
		}
	}

	public Agent createNewAgent() {
		return null;
	}


	// random agents, by default, fileless
	public boolean initAgents() {

	    /* compare with code from new codebase:
	    	SocNetSim2017.SocialNetwork.Agent newAgent = this.socialNetwork.createNewAgent();
			newAgent.getAttributes().setRandomValues(this.random);
			newAgent.setIndex(i);
			this.socialNetwork.addAgent(newAgent, Integer.valueOf(i));

	    */
		for ( int i = 0; i < super.size(); i++ ) {
			Agent new_agent = new Agent(i);
			new_agent.attributes.setRandomValues();
			super.set_vertex(i, new_agent);
		}
		return true;
	}


	// file-based import agents
    public void importPajekAgents() {
        System.out.println("SOCIALNETWORK: importPajekAgents");

        try {
            BufferedReader inFile = new BufferedReader(new FileReader(getPajek_filename()));
            System.out.println("Opening " + getPajek_filename() + " for nodes...");

            String line = null;
            boolean startReading = false;
            int i = 0;
            // NB: PAJEK nodes are 1-based, not 0-based; e.g. [ 1 "0-1-0-0-1" ]
            // therefore largest agent is totalAgents - 1

            int totalAgents = 0;

            while ((line = inFile.readLine()) != null) {

            if (line.startsWith("c statesopts")) {
                StringTokenizer tok = new StringTokenizer(line, " ");
                try {
                    tok.nextToken(); // skip "c"
                    tok.nextToken(); // skip "statesopts"
                    tok.nextToken(); // skip "attno"
                    tok.nextToken(); // attno itself: already read in importPajekGUIValues in SocNetSim2017.GUIControlPanel
                    tok.nextToken(); // skip "valno"
                    tok.nextToken(); // valno itself: already read in importPajekGUIValues in SocNetSim2017.GUIControlPanel
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }

            StringTokenizer st = new StringTokenizer(line);
            String header = st.nextToken();


            if (header.equalsIgnoreCase("*Vertices")) {
                startReading = true;

                // READING TOTAL AGENTS - RESPONSIBILITY OF SocNetSim2017.GUIControlPanel.java -> importPajekGUIValues

                continue;
            }

            if (header.equalsIgnoreCase("*Edges")) {
                startReading = false;
                break;
            }

            if (startReading) {
                Agent new_agent = new Agent(i);
                //SocNetSim2017.SocialNetwork.Agent newAgent = this.socialNetwork.createNewAgent();

                if (st.hasMoreTokens()) {
                    new_agent.attributes.setStringAttributeValues(st.nextToken());

                    //newAgent.getAttributes().fromString(st.nextToken());
                }
                else{
                    new_agent.attributes.setZeroValues();

                    //newAgent.getAttributes().fromString("\"\"");
                }
                new_agent.setIndex(i); // redundant

                //this.sn.addAgent(newAgent, Integer.valueOf(i));
                super.set_vertex(i, new_agent);
                i++;
            }
        }
    } catch (IOException iox) {
        System.out.println("Problem opening " + getPajek_filename());
    }
}
	
	private synchronized void breathFirstSearch(int index) {
		this.unvisited_vertices.clear();
		this.unvisited_vertices.add( super.get_vertex(index) );
		Arrays.fill(this.distance_matrix[index], Integer.MAX_VALUE);
		this.distance_matrix[index][index] = 0;

		while ( this.remaining_vertices.size() > 0 && this.unvisited_vertices.size() > 0 ) {
			Agent current = (Agent) this.unvisited_vertices.remove(0);
			int current_index = current.getIndex();
			
			for ( Enumeration e = current.neighbours.elements(); e.hasMoreElements(); ) {
				Agent neighbour = (Agent) e.nextElement();
				int neighbour_index = neighbour.getIndex();
				if ( this.distance_matrix[index][neighbour.getIndex()] == Integer.MAX_VALUE ) {
					this.unvisited_vertices.add( neighbour );
					this.remaining_vertices.remove( Integer.valueOf( neighbour_index ) );
					this.distance_matrix[index][neighbour_index] = this.distance_matrix[index][current_index] + 1;
				}
			}
		}
	}
	
	private synchronized void calcDistanceMatrixBfs() {
		//System.out.println("calc_distance_matrix BFS");
		
		if ( super.size() != this.distance_matrix.length ) {
			this.distance_matrix = new int[super.size()][super.size()];
		}
		
		for ( int i = 0; i < super.size(); i++ ) {
			for ( int j = i + 1; j < super.size(); j++ ) {
				this.remaining_vertices.put(Integer.valueOf(j), Integer.valueOf(j));
			}
			this.breathFirstSearch(i);
		}
		
		for ( int i = 0; i < this.distance_matrix.length; i++ ) {
			for ( int j = 0; j < this.distance_matrix[i].length; j++ ) {
				this.distance_matrix[j][i] = this.distance_matrix[i][j];
			}
			// System.out.println( Arrays.toString( this.distance_matrix[i] ) );
		}
		//System.out.println("calc_distance_matrix BFS");
	}
	
	private synchronized double averagePathLength() {
		if ( super.size() != this.distance_matrix.length ) {
			this.distance_matrix = new int[super.size()][super.size()];
		}
		
		double sum = 0;
		for ( int i = 0; i < super.size(); i++ ) {
			for ( int j = i + 1; j < super.size(); j++ ) {
				this.remaining_vertices.put(Integer.valueOf(j), Integer.valueOf(j));
			}
			this.breathFirstSearch(i);
			for ( int j = i + 1; j < super.size(); j++ ) {
				if ( this.distance_matrix[i][j] != Integer.MAX_VALUE ) {
					sum += this.distance_matrix[i][j] + this.distance_matrix[i][j];
				}
			}
		}
		
		return ( sum ) / ( super.size() * ( super.size() - 1 ) );
	}
 	
	private synchronized void initMatrices() {
		for ( int i = 0; i < super.matrix.length; i++ ) {
            for ( int j = 0; j < super.matrix[i].length; j++ ) {
                if ( super.matrix[i][j] != null ) {
					this.binary_matrix[i][j] = 1;
				} else {
					this.binary_matrix[i][j] = 0;
				}
			}
		}
	}
	
	/*
	private synchronized void calc_distance_matrix() {
		System.out.println("calc_distance_matrix SocNetSim2017.Utility.Dijkstra");
		this.distance.initAttributes(super.size());
		for ( int i = 0; i < super.size(); i++ ) {
			this.distance.shortest_paths(i, this.binary_matrix, super.size());
			this.distance_matrix[i] = this.distance.get_distance();
			System.out.println( Arrays.toString(this.distance_matrix[i]) );
		}
		System.out.println("calc_distance_matrix SocNetSim2017.Utility.Dijkstra");
	}
	*/
	
	private synchronized void calcSimilarityMatrix() {
		//System.out.println("calcSimilarityMatrix");
		
		if ( super.size() != this.similarity_matrix.length ) {
			this.similarity_matrix = new double[super.size()][super.size()];
		}
		
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			for ( int j = i + 1; j < super.size(); j++ ) {
				Agent b = (Agent) super.get_vertex(j);
				this.similarity_matrix[i][j] = this.similarity_matrix[j][i] = Attributes.calcSimilarity(a.attributes, b.attributes);
			}
			// System.out.println( Arrays.toString(this.similarity_matrix[i]) );
		}
		//System.out.println("calcSimilarityMatrix");
	}

	
	
	private synchronized void distanceSimilarityInteraction() {
		this.calcDistanceMatrixBfs();
		this.calcSimilarityMatrix();
		
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			this.interact_agents.clear();

			for ( int j = 0; j < super.size(); j++ ) {
				if ( this.distance_matrix[i][j] <= this.interaction_radius 
					 && this.similarity_matrix[i][j] >= Attributes.SIMILARITY
					 && i != j ) { 

					this.interact_agents.add((Agent)super.get_vertex(j));
				}
			}

			this.startInteracting(a, this.interact_agents, Agent.INTERACTIONS_PER_TIMESTEP);
		}
	}
	
	private synchronized void similarityInteraction() {
		this.calcSimilarityMatrix();
		
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			this.interact_agents.clear();
			
			for ( int j = 0; j < super.size(); j++ ) {
				if ( this.similarity_matrix[i][j] >= Attributes.SIMILARITY
					 && i != j ) { 

					this.interact_agents.add((Agent)super.get_vertex(j));
				}
			}

			this.startInteracting(a, this.interact_agents, Agent.INTERACTIONS_PER_TIMESTEP);
		}
	}
	
	private synchronized void allInteraction() {
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			this.interact_agents.clear();
			for ( int j = 0; j < super.size(); j++ ) {
				if ( i != j ) { 
					this.interact_agents.add((Agent)super.get_vertex(j));
				}
			}
			
			this.startInteracting(a, this.interact_agents, Agent.INTERACTIONS_PER_TIMESTEP);
		}
	}
	
	private synchronized void distanceInteraction() {
		this.calcDistanceMatrixBfs();
		
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			this.interact_agents.clear();
			for ( int j = 0; j < super.size(); j++ ) {
				if ( this.distance_matrix[i][j] <= this.interaction_radius 
					 && i != j ) { 
					this.interact_agents.add((Agent)super.get_vertex(j));
				}
			}
			this.startInteracting(a, this.interact_agents, Agent.INTERACTIONS_PER_TIMESTEP);
		}
	}
	
	private synchronized void startInteracting(Agent a, ArrayList<Agent> agents, int num_interactions) {
		if ( agents.size() > 0 ) {
			Collections.shuffle(this.interact_agents);
			for ( int curr_int = 0; curr_int < num_interactions; curr_int++ ) {
				a.interact( agents.get(curr_int % agents.size()) );
			}
		}
	}
	
	private void removeEdges() {
		keys.clear();
		for ( Enumeration e = super.edges.elements(); e.hasMoreElements(); ) {
			Edge edge = ((Edge)e.nextElement());
			edge.decreaseWeight();
			if ( edge.getWeight() <= 0 ) {
				keys.add( edge );
			}
		}
		for ( int i = 0; i < keys.size(); i++ ) {
			super.remove_edge( keys.get(i) );
		}
	}
	
	private synchronized void globalInteraction() {
		// this.calcSimilarityMatrix();
		
		for ( int i = 0; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			this.interact_agents.clear();
			
			for ( int j = 0; j < super.size(); j++ ) {
				if ( /* this.similarity_matrix[i][j] >= SocNetSim2017.Attributes.SIMILARITY
					 && */ i != j ) { 

					this.interact_agents.add((Agent)super.get_vertex(j));
				}
			}

			this.startInteracting(a, this.interact_agents, Agent.GLOBAL_INTERACTIONS_PER_TIMESTEP);
		}


	}

	public boolean isTimeStepCurrentlyGlobal() {
		return (this.global_interaction_freq != 0 && (this.current_timesteps % this.global_interaction_freq == 0));
	}
	public synchronized void nextTimeStep() {
		switch ( this.interaction_type ) {
			case SocialNetwork.DISTANCE_SIMILARITY:
				this.distanceSimilarityInteraction();
				// System.out.println(SocNetSim2017.SocialNetwork.DISTANCE_SIMILARITY);
				break;
		
			case SocialNetwork.DISTANCE:
				this.distanceInteraction();
				// System.out.println(SocNetSim2017.SocialNetwork.DISTANCE);
				break;
			
			case SocialNetwork.SIMILARITY:
				// System.out.println(SocNetSim2017.SocialNetwork.SIMILARITY);
				this.similarityInteraction();
				break;
				
			default:
				// System.out.println("default");
				this.allInteraction();
				break;
		}
		
		if ( this.allow_global_interaction && this.isTimeStepCurrentlyGlobal() ) {
			this.globalInteraction();
		}
		
		if ( this.allow_weight ) {
			this.removeEdges();
		}
		
		this.calcMeasurements();
		
		this.current_timesteps++;
	}
	
	public void calcMeasurements() {
		this.initMatrices();
		if ( this.measure_connectivity )
			this.connectivity = Connectivity.getConnectivity(this.binary_matrix, this.binary_matrix.length);
		
		if ( this.measure_CC ) 
			this.clustering_coefficient = ClusteringCoefficient.getClusteringCoefficient(this.binary_matrix, this.binary_matrix.length);
		
		if ( this.measure_modularity )
			this.modularity = Modularity.getModularity(this.binary_matrix, this.binary_matrix.length);
		
		if ( this.measure_apl ) 
			this.average_path_length = this.averagePathLength();
			
		if ( this.measure_plawC || this.measure_correlationC )
			this.calcPowerLaw();
	}
	
	public String measurementsToString() {
		NumberFormat rate_format = NumberFormat.getNumberInstance();
        rate_format.setMinimumFractionDigits(1);
        rate_format.setMaximumFractionDigits(6);
        
        String text = "";
        
		if ( this.measure_connectivity )
			text += "SocNetSim2017.Utility.Connectivity: " + rate_format.format( this.connectivity ) + "\n";
		
		if ( this.measure_CC ) 
			text += "Clustering coefficient: " + rate_format.format( this.clustering_coefficient ) + "\n";
		
		if ( this.measure_modularity )
			text += "SocNetSim2017.Utility.Modularity: " + rate_format.format( this.modularity ) + "\n";
		
		if ( this.measure_apl )
			text += "Average path length: " + rate_format.format( this.average_path_length ) + "\n";
			
		if ( this.measure_plawC )
			text += "Power law coefficient: " + rate_format.format( this.power_law_coefficient ) + "\n";
		
		if ( this.measure_correlationC ) 
			text += "Correlation coefficient: " + rate_format.format( this.correlation_coefficient ) + "\n";
		
		return text;
	}
	
	private void calcPowerLaw() {
		double[] x, y;
		int[] edge_distribution = new int[super.size()];
		int n = 0;
		Arrays.fill(edge_distribution, 0);
		for ( int i = 1; i < super.size(); i++ ) {
			Agent a = (Agent) super.get_vertex(i);
			// System.out.println( a.neighbours.size() );
			edge_distribution[a.neighbours.size()]++;
			
			if ( edge_distribution[a.neighbours.size()] - 1 == 0 ) {
				n++;
			}
		}
		
		int aa = 0;
		x = new double[n];
		y = new double[n];
		//System.out.println("-----------------");
		for ( int i = 1; i < edge_distribution.length; i++ ) {
			if ( edge_distribution[i] > 0 ) {
				x[aa] = Math.log( i );
				y[aa] = Math.log( edge_distribution[i] );
				//System.out.println( i + " " + edge_distribution[i] );
				aa++;
			}
		}
		//System.out.println("-----------------");
		// edge_distribution = null;
		// double m =  SocNetSim2017.Utility.LinearRegression.m(x, y);
		
		if ( this.measure_plawC )
			this.power_law_coefficient = LinearRegression.m(x, y);
		
		if ( this.measure_correlationC ) 
			this.correlation_coefficient  = LinearRegression.r(x, y);
		
		/* System.out.println( Arrays.toString(x) );
		System.out.println( Arrays.toString(y) );
		System.out.println("m: " + m);
		*/ x= null;
		y = null;
		//return m;
	}


}
