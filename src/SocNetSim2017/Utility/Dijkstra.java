package SocNetSim2017.Utility;

/**
 * UTILITY CLASS
 * SocNetSim2017.Utility/Dijkstra.java
 * TODO: not sure where this is used, but refactored to be in SocNetSim2017.Utility as its only apparent use is
 * TODO: to implement the Dijkstra shortest path algo!
 * Created on 13 April 2006
 *
 * @author  Alex Tee Neng Heng
 */

import java.util.*;

public class Dijkstra {
	private LinkedList S;
	private LinkedList<Integer> Q;
	private int[] d;
	private int[] pi;
	private boolean[] added;

	public Dijkstra() {
		this.S = new LinkedList();
		this.Q = new LinkedList<Integer>();
		/* this.d = new int[ num_nodes ];
		this.pi = new int[ num_nodes ];
		this.added = new boolean[ num_nodes ]; */
	}

	public void init(int num_nodes) {
		this.S.clear();
		this.Q.clear();
		this.d = new int[ num_nodes ];
		this.pi = new int[ num_nodes ];
		this.added = new boolean[ num_nodes ];
	}

	public void initialise_single_source(int source, int num_nodes) {
		this.S.clear();
		this.Q.clear();
		for ( int i = 0; i < num_nodes; i++ ) {
			this.d[i] = Integer.MAX_VALUE;
			this.pi[i] = Integer.MIN_VALUE;
			this.added[i] = false;
		}

		this.d[source] = 0;
		this.pi[source] = source;
		for ( int i = 0; i < num_nodes; i++ ) {
			// this.Q.addFirst( new Integer( i ) );
			this.Q.addFirst( Integer.valueOf(i) );
		}
	}	

	public void shortest_paths(int source, int[][] matrix, int num_nodes) {
		/* this->initAttributes( num_nodes ); */
		this.initialise_single_source( source, num_nodes ); // cout << "initialise_single_source" << endl;
		
		while ( !this.Q.isEmpty() ) {
			int u = this.extract_cheapest(); // cout << "extract_cheapest " << endl;
			this.Q.remove( new Integer( u ) ); // cout << "Remove" << endl;

			if ( this.added[ u ] ) {
				for ( int neighbour = 0; neighbour < num_nodes; neighbour++ ) {
					// cout << "(" << u << ", " << neighbour << ")" << endl;
					if ( matrix[u][neighbour] != 0 && u != neighbour ) {
						this.relax( u, neighbour, 1 );	// cout << "Relax" << endl;
					}
				}
			}
		}
	}

	private void relax(int u, int v, int w) {
		if ( this.d[v] > this.d[u] + w ) {
			this.d[v] = this.d[u] + w;
			this.pi[v] = u;
		}
	}

	private int extract_cheapest() {
		// LinkedList w = new LinkedList();
		int min_distance = Integer.MAX_VALUE;
		int cheapest = -1;
		
		/*
		for ( w = this->Q.begin(); w != this->Q.end(); ++w ) {
			// cout << this->d[*w] << " ";
			if ( this->d[*w] < min_distance ) {
				cheapest = *w;
				min_distance = this->d[*w];
			}
		}
		*/
		
		for ( int i = 0; i < this.Q.size(); i++ ) {
			Integer w = (Integer) this.Q.get( i );

			if ( this.d[ w.intValue() ] < min_distance ) {
				cheapest = w.intValue();
				min_distance = this.d[ w.intValue() ];
			}
		}
		// cout << endl;

		if ( cheapest != -1 ) {
			this.added[ cheapest ] = true;
			return cheapest;
		} else {
			int front = ((Integer) this.Q.getFirst()).intValue();
			this.added[ front ] = false;
			return front;
		}
	}

	public double average_shortest_path_length(int[][] matrix, int num_nodes) {
		double sum_pl = 0.0;
		
		this.init( num_nodes );	// cout << "dfsdfsdf" << endl;
		for ( int i = 0; i < num_nodes; i++ ) {
			// cout << i << " ";
			this.shortest_paths(i, matrix, num_nodes);
			for ( int j = 0; j < num_nodes; j++ ) {
				// cout << "--- " << this->d[j] << endl;
				if ( this.added[ j ] ) {
					// cout << j << " --- " << this->d[j] << " | ";
					sum_pl += this.d[j];
				} 
				/*
				else {
					cout << "----" << endl;
				}
				*/
			}
			// cout << endl;
		}
		// cout << endl;	

		return ( sum_pl ) / ( num_nodes * ( num_nodes - 1 ) );
	}
	
	public int[] get_distance() {
		return this.d;
	}
}
