package SocNetSim2017.Utility;

/**
 * UTILITY CLASS
 * SocNetSim2017.Utility/Modularity.java
 *
 * Created on 13 April 2006
 *
 * @author  Alex Tee Neng Heng
 */


public class Modularity {

	public Modularity() { }

	public static double getModularity(int[][] matrix, int size) {
		double S = 0;
		
		for ( int i = 0; i < size; i++ ) {
			for ( int j = 0; j < size; j++ ) {
				if ( i != j ) {
					double s_ij = Modularity.calcS( matrix[i], matrix[j], size );
					S += s_ij;
				}
			}
		}

		return S * 1.0 / ( size * ( size - 2 ) );
	}

	private static double calcS(int[] a, int[] b, int size) {
		double common = 0, 
          	   total = 0;
        
		for ( int i = 0; i < size; i++ ) {
			if ( a[i] != 0 && b[i] != 0 ) {
				common++;
				total++;
			} else if ( ( a[i] != 0 && b[i] == 0 ) || ( a[i] == 0 && b[i] != 0 ) ) {
				total++;
			}
		}
		
		if ( total > 0 ) {
			return common / total;
		} else {
			return 0;
		}
	}
}
