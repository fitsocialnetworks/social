package SocNetSim2017.Utility;

import java.util.*;
import SocNetSim2017.*;

/**
 * Created by marc on 13/04/2017.
 */
public class PajekExporter {

    private static String printAgentAttributes(Vertex node) {
        if (! (node instanceof Agent))
            return "";


        return '"' + ((Agent) node).attributes.toStringWithDashes() + '"';
    }

    public static List<String> exportStrings(Graph g, Attributes a) {
        List<String> result = new ArrayList<String>();

        String attNo = a.NUM_ATTRIBUTES + "";
        String valNo = a.NUM_VALUES + "";

        result.add("c statesopts attno " + attNo + " valno " + valNo);



        Hashtable<Integer, Vertex> vertices = g.get_vertices();
        Hashtable<String, Edge> edges = g.get_all_edges();

        result.add("*Vertices " + vertices.size());

        // WARNING: the internal contents are zero-based...
        // ... the written contents are one-based
        for (int index = 0; index < vertices.size(); index++) {
            result.add("" + (index+1) + " " + printAgentAttributes(vertices.get(index)));
            //indexes.put(v, Integer.valueOf(index++));
        }

        result.add("*Edges");

        // WARNING: the internal contents are zero based...
        // ... the written contents are one-based

        for (Edge e : edges.values() ) {
            int source = e.p1.getIndex() + 1;
            int target = e.p2.getIndex() + 1;

            result.add(source + " " + target);
        }

        return result;

    }
}

