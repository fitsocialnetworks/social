package SocNetSim2017.Utility;

/**
 * UTILITY CLASS
 * SocNetSim2017.Utility/Connectivity.java
 *
 * Created on 13 April 2006
 *
 * @author  Alex Tee Neng Heng
 **/

public class Connectivity {

	public Connectivity() { }

	public static double getConnectivity(int[][] matrix, int size) {
		int num_edges = 0;

		for ( int i = 0; i < size; i++ ) {
			for ( int j = 0; j < size; j++ ) {
				if ( matrix[i][j] != 0 && i != j ) {
					num_edges++;
				}
			}
		}

		if ( (Math.pow(size, 2) - size) != ( (size * size) - size ) ) {
			System.err.println("ERROR!");
			System.exit(1);
		}
		
		return num_edges * 1.0 / ( Math.pow( size, 2 ) - size );
	}
}
