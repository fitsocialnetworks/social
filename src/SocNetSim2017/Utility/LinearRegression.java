package SocNetSim2017.Utility;

/**
 * UTILITY CLASS
 * SocNetSim2017.Utility/LinearRegression.java
 *
 */

public class LinearRegression {
	
	public static double m(double[] x, double[] y) {
		double sum_xy = 0,
			   sum_x = 0,
			   sum_y = 0,
			   sum_x2 = 0;
		
		for ( int i = 0; i < x.length; i++ ) {
			sum_xy += (x[i] * y[i]);
			sum_x += x[i];
			sum_y += y[i];
			sum_x2 += Math.pow(x[i], 2);
		}
		
		return ( x.length * sum_xy - sum_x * sum_y ) / ( x.length * sum_x2 - sum_x * sum_x );
	}
	
	public static double b(double[] x, double[] y, double m) {
		double sum_x = 0,
		   	   sum_y = 0;
		
		for ( int i = 0; i < x.length; i++ ) {
			sum_x += x[i];
			sum_y += y[i];
		}
		   	      	   
		return ( sum_y - m * sum_x ) / x.length;
	}
	
	public static double r(double[] x, double[] y) {
		double sum_xy = 0,
		   sum_x = 0,
		   sum_y = 0,
		   sum_x2 = 0,
		   sum_y2 = 0;
		
		for ( int i = 0; i < x.length; i++ ) {
			sum_xy += (x[i] * y[i]);
			sum_x += x[i];
			sum_y += y[i];
			sum_x2 += Math.pow(x[i], 2);
			sum_y2 +=  Math.pow(y[i], 2);
		}
		
		return ( (x.length * sum_xy) - (sum_x * sum_y) ) / Math.sqrt( (x.length * sum_x2 - Math.pow(sum_x, 2) ) * (x.length * sum_y2 - Math.pow(sum_y, 2) ) );
	}
}
