package SocNetSim2017.Utility;

/**
 * UTILITY CLASS
 * SocNetSim2017.Utility/ClusteringCoefficient.java
 *
 * Created on 13 April 2006
 *
 * @author  Alex Tee Neng Heng
 */


public class ClusteringCoefficient {
	
	public ClusteringCoefficient() { }

	public static double getClusteringCoefficient(int[][] matrix, int num_nodes) {
		double sum_Ci = 0;

		for ( int i = 0; i < num_nodes; i++ ) {
			double cc = ClusteringCoefficient.calcNodeCC(i, matrix, num_nodes);
			sum_Ci += cc;
		}

		return sum_Ci / num_nodes;
	}

	private static double calcNodeCC(int index, int[][] matrix, int num_nodes) {
		int N = 0;

		int[] neighbours = new int[1000];
		int num_neighbours = 0;
		for ( int i = 0; i < num_nodes; i++ ) {
			if ( matrix[index][i] != 0 && index != i ) {
				neighbours[num_neighbours] = i;
				num_neighbours++;
			}
		}

		if ( num_neighbours <= 1 ) {
			return 0;
		}
		
		for ( int i = 0; i < num_neighbours; i++ ) {
			for ( int j = 0; j < i; j++ ) {
				if ( matrix[ neighbours[i] ][ neighbours[j] ] != 0 ) {
					N++;
				}	
			}
		}

		// cout << "index: " << index <<  " N: " << N << endl;
		
		return ( N ) / ( num_neighbours * ( num_neighbours - 1 ) / 2.0 );

		// return ( N * 2.0 ) / ( neighbours.size() * ( neighbours.size() - 1 ) );
	}
}
