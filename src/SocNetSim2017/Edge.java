package SocNetSim2017;

/**
 * DATA STRUCTURE
 * SocNetSim2017.Edge
 * Base class to capture an edge data structure
 * UPPERCASE statics are universal 'constants' mutable by initGlobalEdge
 *
 * Created on 6 October 2005
 * @author  Alex Tee Neng Heng
 */


public class Edge {
	public static double WEIGHT = 2;
	public static double WEIGHT_INCREASE = 1;
	public static double WEIGHT_DECREASE = 1;
	
    public Vertex p1;
    public Vertex p2;

	private double weight;

	public Edge(Vertex p_1, Vertex p_2) {
		this.p1 = p_1;
		this.p2 = p_2;
		this.setWeight(Edge.WEIGHT);
	}
	
	public Edge(Vertex p_1, Vertex p_2, double w) {
		this.setWeight(w);
		this.p1 = p_1;
		this.p2 = p_2;
	}
	
	public String getIDString() {
		return "" + this.p1.getIndex() + this.p2.getIndex();
	}
	
	public void setWeight(double w) {
		this.weight = w;
	}
	
	public void increaseWeight() {
		this.weight += Edge.WEIGHT_INCREASE;
	}
	
	public void decreaseWeight() {
		this.weight -= Edge.WEIGHT_DECREASE;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public String toString() {
		return this.getIDString() + ": (" + this.p1.getIndex() + ", " + this.p2.getIndex() + ")";
	} 
	
	public static void initGlobalEdge(double W, double W_I, double W_D) {
		Edge.WEIGHT = W;
		Edge.WEIGHT_INCREASE = W_I;
		Edge.WEIGHT_DECREASE = W_D;
	}
}
