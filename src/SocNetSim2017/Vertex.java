package SocNetSim2017;
/**
 * DATA STRUCTURE
 * SocNetSim2017.Vertex
 * Base class to capture a vertex data structure
 *
 * Created on 6 October 2005
 * @author  Alex Tee Neng Heng
 **/

import java.util.*;

public class Vertex {
	private int index;
	public Hashtable<Integer, Vertex> neighbours;
	
	public Vertex() {
		this.setIndex(0);
		this.neighbours = new Hashtable<Integer, Vertex>();
	}
	
	public Vertex(int index) {
		this.setIndex(index);
		this.neighbours = new Hashtable<Integer, Vertex>();
	}

	public void setIndex(int num) {
		if ( num >= 0 ) {
			this.index = num;
		} else {
			System.err.println("number: " + num + ". number cannot be less than 0");
			System.exit(1);
		}
	}

	public int getIndex() {
		return this.index;
	}
	
	public String toString() {
		String text = "Index: " + this.index;
		text += " Neighbours: [";
		for (Enumeration e = this.neighbours.elements(); e.hasMoreElements();) {
	         text += " " + ((Vertex)e.nextElement()).getIndex() + " ";
	    }
		text += " ]";
		return text;
	}
}
