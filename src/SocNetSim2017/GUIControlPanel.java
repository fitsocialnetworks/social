package SocNetSim2017;

import SocNetSim2017.Utility.PajekExporter;

import java.awt.*;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.*;
import java.awt.event.*;
import java.util.StringTokenizer;


/**
 * SocNetSim2017.GUIControlPanel - GUI Java App
 * has a main to start simulation
 **/


public class GUIControlPanel extends javax.swing.JPanel {
	private final int[] SPEED = {750, 500, 250, 100, 50, 25};
	private static final Font FONT = new java.awt.Font("Lucida Grande", 0, 12);
	
	private JPanel control_panel;
	private JButton play_button;
	private JButton reset_button;
	private JComboBox speed_combobox;
    private JLabel speed_label;
    private JButton start_button;
    private JButton step_button;
    
    private JTabbedPane parameters_tabbedpane;
    
    private JPanel interaction_panel;
    private JFormattedTextField num_interactions_textfield;
	private JLabel num_interactions_label;
	private JLabel distance_label;
	private JFormattedTextField distance_textfield;
	
	private JPanel attributes_panel;
	private JLabel num_attributes_label;
	private JFormattedTextField num_attributes_textfield;
	private JLabel attr_num_values_label;
    private JFormattedTextField attr_num_values_textfield;
    private JComboBox distance_type_combobox;
    private JLabel distance_togglebutton_label;
    private JToggleButton distance_togglebutton;
	
    private JPanel influence_panel;
    private JLabel influence_label;
    private JFormattedTextField min_influence_textfield;
    private JFormattedTextField max_influence_textfield;
	private JLabel influence_togglebutton_label;
    private JToggleButton influence_togglebutton;
    
    private JPanel edge_panel;
    private JLabel weight_label;
    private JFormattedTextField weight_textfield;
    private JLabel weight_increase_label;
    private JFormattedTextField weight_increase_textfield;
    private JLabel weight_decrease_label;
    private JFormattedTextField weight_decrease_textfield;
    private JLabel weight_togglebutton_label;
    private JToggleButton weight_togglebutton;
    
    private JPanel similarity_panel;
    private JLabel similarity_label;
    private JFormattedTextField similarity_textfield;
	private JLabel similarity_togglebutton_label;
	private JToggleButton similarity_togglebutton;

    private JPanel utilities_panel;
    private JButton pajekExportButton;


	private JTabbedPane network_tabbedpane;
	private JPanel random_panel;
	private JPanel smallworld_panel;
	private JPanel tree_panel;
	private JPanel scalefree_panel;
	private JLabel connectivity_label;
	private JFormattedTextField connectivity_textfield;
	private JLabel shortcut_prob_label;
    private JFormattedTextField shortcut_prob_textfield;
    private JLabel num_branches_label;
    private JFormattedTextField num_branches_textfield;
    private JLabel k_label;
    private JFormattedTextField k_textfield;
    private JLabel num_nodes_label;
    private JFormattedTextField num_nodes_textfield;
    
    private JPanel global_interactions_panel;
    private JLabel num_global_interactions_label;
    private JFormattedTextField num_global_interactions_textfield;
    // private JLabel global_similarity_label;
    // private JFormattedTextField global_similarity_textfield;
	private JLabel global_similarity_togglebutton_label;
	private JToggleButton global_similarity_togglebutton;
	private JLabel global_interactions_freq_label;
	private JFormattedTextField global_interactions_freq_textfield;
    
    private JTextArea stat_textarea;
    
    private JPanel display_panel;
    private JCheckBox network_panel_checkbox;
    private JCheckBox graph_panel_checkbox;
    
    private JPanel measurements_panel;
    private JCheckBox measure_connectivity_checkbox;
    private JCheckBox measure_CC_checkbox;
    private JCheckBox measure_modularity_checkbox;
    private JCheckBox measure_apl_checkbox;
    private JCheckBox measure_plawC_checkbox;
    private JCheckBox measure_correlationC_checkbox;

    private JPanel import_panel;
    private JLabel import_label;
    private JButton pajekLoadButton;
    private JTextField pajekTextfield;
    private JFileChooser pajekFilePicker;

	private SimThread sim_thread;
	
	private SocialNetwork sn;
	private GUISocialNetworkPanel sn_panel;
	private DialogBox sn_panel_dialog;
	private DialogBox sn_graph_dialog;
	
	private NumberFormat rate_format;
    /*
	{
		//Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("apple.laf.AquaLookAndFeel");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new GUIControlPanel());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("SocNetSim 2017");

        // <div>Icons made by <a href="http://www.flaticon.com/authors/prosymbols" title="Prosymbols">Prosymbols</a>
        // from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>
        // is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
        // title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        String path = System.getProperty("user.dir", "") + "/icon.png";

        if (System.getProperty("os.name").contains("Windows"))
            frame.setIconImage(Toolkit.getDefaultToolkit().getImage(path));
        else if (System.getProperty("os.name").contains("Mac"))
            com.apple.eawt.Application.getApplication().setDockIconImage(Toolkit.getDefaultToolkit().getImage(path));


        frame.pack();
		frame.setVisible(true);
	}
	
	public GUIControlPanel() {
		super();



		this.rate_format = NumberFormat.getNumberInstance();
        this.rate_format.setMinimumFractionDigits(1);
        this.rate_format.setMaximumFractionDigits(6);
        
		Attributes.initAttributes(5, 2, Attributes.HAMMING_DISTANCE, 50);
		
		this.sn = new SocialNetwork(100);
		this.sn_panel = new GUISocialNetworkPanel(this.sn, 700, 700);
		
		Agent.MY_SN = this.sn;
		initGUI();
		
        this.toggleButtons(false);
        this.togglePauseButton(false);
        this.sim_thread = new SimThread("Thread");
	}
	
	private void initGUI() {
		try {
			this.setLayout(new java.awt.GridBagLayout());
			this.setFont(new java.awt.Font("Monaco", 0, 11));
			{
				this.stat_textarea = this.createNewTextarea(10, 36, "", true, false);
				this.sn_graph_dialog = new DialogBox(new JFrame(), this.stat_textarea, "Statistic");
				this.sn_graph_dialog.addWindowListener(new WindowAdapter() {
				    public void windowClosing(WindowEvent evt) {
				    	sn_graph_dialogwindowClosing(evt);
				    }
				});
				this.sn_panel_dialog = new DialogBox(new JFrame(), this.sn_panel, "Social network");
				this.sn_panel_dialog.addWindowListener(new WindowAdapter() {
				    public void windowClosing(WindowEvent evt) {
				    	sn_panel_dialogwindowClosing(evt);
				    }
				});
			}
			{
				this.createNetworkPanel();
				this.add(this.network_tabbedpane, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
			}
			{
				this.createParametersPanel();
				this.add(this.parameters_tabbedpane, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
			}
			{
				this.createControlPanel();
		        this.add(this.control_panel, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
			}
			{
				this.createDisplayPanel();
		        this.add(this.display_panel, this.createNewGridBagConstraints(0, 3, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
			}
			{
				this.createMeasurementsPanel();
		        this.add(this.measurements_panel, this.createNewGridBagConstraints(0, 4, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
			}
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createParametersPanel() {
		this.parameters_tabbedpane = new JTabbedPane(JTabbedPane.TOP);
		this.parameters_tabbedpane.setFont(GUIControlPanel.FONT);
		this.parameters_tabbedpane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
		
		this.createAttributesPanel();
		this.parameters_tabbedpane.addTab("Attributes", this.attributes_panel);
		
		this.createSimilarityPanel();
		this.parameters_tabbedpane.addTab("Similarity", this.similarity_panel);
		
		this.createInteractionPanel();
		this.parameters_tabbedpane.addTab("Local interaction", this.interaction_panel);
		
		this.createGlobalInteractionsPanel();
		this.parameters_tabbedpane.addTab("Global interaction", this.global_interactions_panel);
		
		this.createEdgePanel();
		this.edge_panel.setPreferredSize(new Dimension(400, 100));
		this.parameters_tabbedpane.addTab("Edge", this.edge_panel);
		
		this.createInfluencePanel();
		this.parameters_tabbedpane.addTab("Influence", this.influence_panel);


        this.createUtilitiesPanel();;
        this.parameters_tabbedpane.addTab("Utilities", this.utilities_panel);
	}
	
	private void createNetworkPanel() {
		this.network_tabbedpane = new JTabbedPane();
		this.network_tabbedpane.setFont(GUIControlPanel.FONT);
		this.network_tabbedpane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                network_tabbed_paneStateChanged(evt);
            }
        });
		
		this.num_nodes_label = this.createNewLabel("No. of agents ", this.num_nodes_textfield);
		this.num_nodes_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(1000)), 5, new Integer(100));

		this.random_panel = new JPanel();
		this.random_panel.setLayout(new java.awt.GridBagLayout());
		
		this.connectivity_label = this.createNewLabel("Connectivity ", this.connectivity_textfield);
		this.random_panel.add(this.connectivity_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.connectivity_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(1), 1, 5), 5, new Double(0.2));
		this.random_panel.add(this.connectivity_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.network_tabbedpane.add("Random", this.random_panel);
		
		this.smallworld_panel = new JPanel();
		// this.smallworld_panel.setPreferredSize(new Dimension(350, 100));
		this.smallworld_panel.setLayout(new java.awt.GridBagLayout());
		
		this.k_label = this.createNewLabel("Nearest neighbours, k ", this.k_textfield);
		this.smallworld_panel.add(this.k_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.k_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(2), new Integer(10)), 5, new Integer(2));
		this.smallworld_panel.add(this.k_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	
		this.shortcut_prob_label = this.createNewLabel("Shortcut probability", this.shortcut_prob_textfield);
		this.smallworld_panel.add(this.shortcut_prob_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.shortcut_prob_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(1), 1, 5), 5, new Double(0.05));
		this.smallworld_panel.add(this.shortcut_prob_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	
		this.network_tabbedpane.add("Small world", this.smallworld_panel);
		
		this.scalefree_panel = new JPanel();
		this.scalefree_panel.setLayout(new java.awt.GridBagLayout());
		
		this.network_tabbedpane.add("Scale free", this.scalefree_panel);
		
		this.tree_panel = new JPanel();
		this.tree_panel.setLayout(new java.awt.GridBagLayout());
		
		this.num_branches_label = this.createNewLabel("No. of branches ", this.num_branches_textfield);
		this.tree_panel.add(this.num_branches_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.num_branches_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(2), new Integer(5)), 5, new Integer(2));
		this.tree_panel.add(this.num_branches_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	
		this.network_tabbedpane.add("Tree", this.tree_panel);



        this.import_label = this.createNewLabel("Select Pajek file to import", this.pajekLoadButton);

        this.pajekFilePicker = new JFileChooser();
        this.pajekFilePicker.setCurrentDirectory(new File(System.getProperty("user.dir", "")));
        this.pajekFilePicker.setFileFilter(new FileFilter() {
            public boolean accept(File f) {
                // reject nulls, ??? directory
                if (f == null) return false;
                if (f.isDirectory()) return true;
                int p = f.getName().lastIndexOf(".");

                if (p<0) return false;

                String ext = f.getName().substring(p);
                return ext.equalsIgnoreCase(".pajek") || ext.equalsIgnoreCase(".net");
            }

            public String getDescription() {
                return "Pajek format for SocNetSim (*.pajek | *.net)";
            }
        });

        this.pajekLoadButton = this.createNewButton("Browse");
        this.pajekLoadButton.addActionListener(new java.awt.event.ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               pajekFilePicker_ActionPerformed(e);
           }
        });



        this.import_panel = new JPanel();
        this.import_panel.setLayout(new java.awt.GridBagLayout());

        this.import_panel.add(this.import_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
        this.import_panel.add(this.pajekLoadButton, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

        this.pajekTextfield = new JTextField(20);
        this.pajekTextfield.setEditable(false); // prevent user tampering - must use filepicker
        this.import_panel.add(this.pajekTextfield, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

		this.network_tabbedpane.add("Import from Pajek", this.import_panel);



	}
	
	private void createDisplayPanel() {
		this.display_panel = new JPanel();
		this.display_panel.setLayout(new java.awt.GridBagLayout());
		this.display_panel.setBorder(BorderFactory.createTitledBorder("Display"));
		
		this.network_panel_checkbox = this.createNewCheckbox("Show network", false);
		this.network_panel_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				network_panel_checkboxitemStateChanged(evt);
			}
		});
		this.display_panel.add(this.network_panel_checkbox, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.graph_panel_checkbox = this.createNewCheckbox("Show graph", false);
		this.graph_panel_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				graph_panel_checkboxitemStateChanged(evt);
			}
		});
		this.display_panel.add(this.graph_panel_checkbox, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}
    
	private void createMeasurementsPanel() {
		this.measurements_panel = new JPanel();
		this.measurements_panel.setLayout(new java.awt.GridBagLayout());
		this.measurements_panel.setBorder(BorderFactory.createTitledBorder("Measurements"));
		
		this.measure_connectivity_checkbox = this.createNewCheckbox("Connectivity", true);
		this.measure_connectivity_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_connectivity_checkbox, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.measure_CC_checkbox = this.createNewCheckbox("Clustering coefficient", true);
		this.measure_CC_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_CC_checkbox, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.measure_modularity_checkbox = this.createNewCheckbox("Modularity", true);
		this.measure_modularity_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_modularity_checkbox, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.measure_apl_checkbox = this.createNewCheckbox("Average path length", true);
		this.measure_apl_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_apl_checkbox, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.measure_plawC_checkbox = this.createNewCheckbox("Power law coefficient", true);
		this.measure_plawC_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_plawC_checkbox, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.measure_correlationC_checkbox = this.createNewCheckbox("Correlation coefficient", true);
		this.measure_correlationC_checkbox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt){
				measurements_checkboxitemStateChanged(evt);
			}
		});
		this.measurements_panel.add(this.measure_correlationC_checkbox, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
	}
	
	private void createSimilarityPanel() {
		this.similarity_panel = new JPanel();
		this.similarity_panel.setLayout(new java.awt.GridBagLayout());

		this.similarity_togglebutton_label = this.createNewLabel("Similarity ", this.similarity_togglebutton);
		this.similarity_panel.add(this.similarity_togglebutton_label, this.createNewGridBagConstraints(0, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.similarity_togglebutton = this.createNewToggle("On", true);
		this.similarity_togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	similarity_togglebuttonActionPerformed(evt);
            }
        });
		this.similarity_panel.add(this.similarity_togglebutton, this.createNewGridBagConstraints(1, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.similarity_label = this.createNewLabel("Similarity (0-100%) ", this.similarity_textfield);
		this.similarity_panel.add(this.similarity_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.similarity_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(100)), 5, new Double(20));
		this.similarity_panel.add(this.similarity_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.distance_type_combobox = new JComboBox(); 
		this.distance_type_combobox.setFont(GUIControlPanel.FONT);
		this.distance_type_combobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Hamming distance", "Manhattan distance", "Euclidean distance"}));
		this.similarity_panel.add(this.distance_type_combobox, this.createNewGridBagConstraints(0, 3, 2, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}
	
	/*
    private JPanel global_interactions_panel;
    private JLabel num_global_interactions_label;
    private JFormattedTextField num_global_interactions_textfield;
    private JLabel global_similarity_label;
    private JFormattedTextField global_similarity_textfield;
	private JLabel global_similarity_togglebutton_label;
	private JToggleButton global_similarity_togglebutton;
	*/
	
	private void createGlobalInteractionsPanel() {
		this.global_interactions_panel = new JPanel();
		this.global_interactions_panel.setLayout(new java.awt.GridBagLayout());
		
		this.global_similarity_togglebutton_label = this.createNewLabel("Similarity ", this.similarity_togglebutton);
		this.global_interactions_panel.add(this.global_similarity_togglebutton_label, this.createNewGridBagConstraints(0, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.global_similarity_togglebutton = this.createNewToggle("On", true);
		this.global_similarity_togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	similarity_togglebuttonActionPerformed(evt);
            }
        });
		this.global_interactions_panel.add(this.global_similarity_togglebutton, this.createNewGridBagConstraints(1, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

		// private JLabel global_interactions_freq_label;
		// private JFormattedTextField global_interactions_freq_textfield;
		
		this.global_interactions_freq_label = this.createNewLabel("Frequency/period (timesteps)", this.global_interactions_freq_textfield);
		this.global_interactions_panel.add(this.global_interactions_freq_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.global_interactions_freq_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(100)), 5, new Integer(5));
		this.global_interactions_panel.add(this.global_interactions_freq_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.num_global_interactions_label = this.createNewLabel("Interactions/timestep for each agent ", this.num_global_interactions_textfield);
		this.global_interactions_panel.add(this.num_global_interactions_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.num_global_interactions_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(100)), 5, new Integer(1));
		this.global_interactions_panel.add(this.num_global_interactions_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

		/*
		this.global_similarity_label = this.createNewLabel("Similarity ", this.global_similarity_textfield);
		this.global_interactions_panel.add(this.global_similarity_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.global_similarity_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(100)), 5, new Double(20));
		this.global_interactions_panel.add(this.global_similarity_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
*/
	}
	
	private void createEdgePanel() {
		this.edge_panel = new JPanel();
		this.edge_panel.setLayout(new java.awt.GridBagLayout());
		
		this.weight_togglebutton_label = this.createNewLabel("Weight ", this.weight_togglebutton);
		this.edge_panel.add(this.weight_togglebutton_label, this.createNewGridBagConstraints(0, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.weight_togglebutton = this.createNewToggle("On", true);
		this.weight_togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	weight_togglebuttonActionPerformed(evt);
            }
        });
		this.edge_panel.add(this.weight_togglebutton, this.createNewGridBagConstraints(1, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.weight_label = this.createNewLabel("Initial weight ", this.weight_textfield);
		this.edge_panel.add(this.weight_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.weight_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(1), new Double(100)), 5, new Double(Edge.WEIGHT));
		this.edge_panel.add(this.weight_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.weight_increase_label = this.createNewLabel("Weight increase ", this.weight_increase_textfield);
		this.edge_panel.add(this.weight_increase_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.weight_increase_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(1), new Double(100)), 5, new Double(Edge.WEIGHT_INCREASE));
		this.edge_panel.add(this.weight_increase_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.weight_decrease_label = this.createNewLabel("Weight decrease ", this.weight_decrease_label);
		this.edge_panel.add(this.weight_decrease_label, this.createNewGridBagConstraints(0, 3, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.weight_decrease_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(1), new Double(100)), 5, new Double(Edge.WEIGHT_DECREASE));
		this.edge_panel.add(this.weight_decrease_textfield, this.createNewGridBagConstraints(1, 3, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}
	
	private void createAttributesPanel() {
		this.attributes_panel = new JPanel();
		this.attributes_panel.setLayout(new java.awt.GridBagLayout());
		
		this.num_attributes_label = this.createNewLabel("No. of attributes ", this.num_attributes_textfield);
		this.attributes_panel.add(this.num_attributes_label, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.num_attributes_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(100)), 5, new Integer(5));
		this.attributes_panel.add(this.num_attributes_textfield, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.attr_num_values_label = this.createNewLabel("No. of values ", this.attr_num_values_textfield);
		this.attributes_panel.add(this.attr_num_values_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.attr_num_values_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(100)), 5, new Integer(2));
		this.attributes_panel.add(this.attr_num_values_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	
	}
	
	private void createInfluencePanel() {
		this.influence_panel = new JPanel();
		this.influence_panel.setLayout(new java.awt.GridBagLayout());
		
		this.influence_togglebutton_label = this.createNewLabel("Influence ", this.influence_togglebutton);
		this.influence_panel.add(this.influence_togglebutton_label, this.createNewGridBagConstraints(0, 0, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.influence_togglebutton = this.createNewToggle("On", true);
		this.influence_togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	influence_togglebuttonActionPerformed(evt);
            }
        });
		this.influence_panel.add(this.influence_togglebutton, this.createNewGridBagConstraints(1, 0, 2, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.influence_label = this.createNewLabel("Influence (Min - Max) ", this.min_influence_textfield);
		this.influence_panel.add(this.influence_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.min_influence_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(1)), 5, new Double(0.5));
		this.influence_panel.add(this.min_influence_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.max_influence_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Double(0), new Double(1)), 5, new Double(0.5));
		this.influence_panel.add(this.max_influence_textfield, this.createNewGridBagConstraints(2, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}
	
	private void createInteractionPanel() {
		this.interaction_panel = new JPanel();
		this.interaction_panel.setLayout(new java.awt.GridBagLayout());
		
		this.distance_togglebutton_label = this.createNewLabel("Distance", this.distance_togglebutton);
		this.interaction_panel.add(this.distance_togglebutton_label, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.distance_togglebutton = this.createNewToggle("On", true);
		this.distance_togglebutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	distance_togglebuttonActionPerformed(evt);
            }
        });
		this.interaction_panel.add(this.distance_togglebutton, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		
		this.distance_label = this.createNewLabel("Distance ", this.distance_textfield);
		this.interaction_panel.add(this.distance_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.distance_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(1000)), 5, new Integer(1));
		this.interaction_panel.add(this.distance_textfield, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

		this.num_interactions_label = this.createNewLabel("Interactions/timestep for each agent ", this.num_interactions_textfield);
		this.interaction_panel.add(this.num_interactions_label, this.createNewGridBagConstraints(0, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
		this.num_interactions_textfield = this.createNewTextfield(this.createNewNumberFormatter(new Integer(1), new Integer(1000)), 5, new Integer(1));
		this.interaction_panel.add(this.num_interactions_textfield, this.createNewGridBagConstraints(1, 2, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}

	private void createUtilitiesPanel() {
        this.pajekExportButton = this.createNewButton("Export");
        this.pajekExportButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                java.util.List<String> ls = PajekExporter.exportStrings(sn, new Attributes() );

                System.out.println("=====================");
                for (String s: ls)
                    System.out.println(s);
                System.out.println("=====================");

            }
        });

        this.utilities_panel  = new JPanel();
        this.utilities_panel.setLayout(new java.awt.GridBagLayout());
        this.utilities_panel.add(pajekExportButton, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
    }

	private void createControlPanel() {
		control_panel = new javax.swing.JPanel();
        speed_combobox = new javax.swing.JComboBox();

		control_panel.setLayout(new java.awt.GridBagLayout());
		control_panel.setBorder(BorderFactory.createTitledBorder("Border"));

		start_button = this.createNewButton("Start");
        start_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                start_buttonActionPerformed(evt);
            }
        });
        control_panel.add(start_button, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE));

        reset_button = this.createNewButton("Reset");
        reset_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reset_buttonActionPerformed(evt);
            }
        });
        control_panel.add(reset_button, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE));

        play_button = this.createNewButton("Play");
        play_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                play_buttonActionPerformed(evt);
            }
        });
        control_panel.add(play_button, this.createNewGridBagConstraints(2, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE));

        step_button = this.createNewButton("Step");
        step_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                step_buttonActionPerformed(evt);
            }
        });
        control_panel.add(step_button, this.createNewGridBagConstraints(3, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE));

        speed_label = this.createNewLabel("Speed", speed_combobox);
        control_panel.add(speed_label, this.createNewGridBagConstraints(0, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE));

        speed_combobox.setFont(GUIControlPanel.FONT);
        speed_combobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4"}));
        speed_combobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                speed_comboboxActionPerformed(evt);
            }
        });
        control_panel.add(speed_combobox, this.createNewGridBagConstraints(1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
	}

	private void pajekFilePicker_ActionPerformed(java.awt.event.ActionEvent e)
    {
        if (JFileChooser.APPROVE_OPTION != pajekFilePicker.showOpenDialog(getParent()) )
                return;

        File selFile = pajekFilePicker.getSelectedFile();

        if (!selFile.exists())
            return;

		// lock and force user to knowingly reset
        start_button.setEnabled(false);
        pajekTextfield.setText(selFile.getAbsolutePath());
        JOptionPane.showMessageDialog(null,"Please remember to RESET the simulation to...\n"+
                "- GUI: load textboxes with attribute data/values and number of nodes.\n"+
                "- SocialNetwork: init, then reload all agents and attribs from file data.\n"+
                "- Network: init, then reload all Vertex and Edge data.","File loaded",JOptionPane.INFORMATION_MESSAGE);


    }

	private void similarity_togglebuttonActionPerformed(java.awt.event.ActionEvent evt) {
		if ( this.similarity_togglebutton.isSelected() ) {
			this.similarity_textfield.setEditable(true);
			this.distance_type_combobox.setEnabled(true);
		} else {
			this.similarity_textfield.setEditable(false);
			this.distance_type_combobox.setEnabled(false);
		}
    }
	
	private void distance_togglebuttonActionPerformed(java.awt.event.ActionEvent evt) {
		if ( this.distance_togglebutton.isSelected() ) {
			this.distance_textfield.setEditable(true);
		} else {
			this.distance_textfield.setEditable(false);
		}
    }
	
	private void influence_togglebuttonActionPerformed(java.awt.event.ActionEvent evt) {
		if ( this.influence_togglebutton.isSelected() ) {
			this.min_influence_textfield.setEditable(true);
			this.max_influence_textfield.setEditable(true);
		} else {
			this.min_influence_textfield.setEditable(false);
			this.max_influence_textfield.setEditable(false);
		}
    }
	
	private void weight_togglebuttonActionPerformed(java.awt.event.ActionEvent evt) {
		if ( this.weight_togglebutton.isSelected() ) {
			this.weight_textfield.setEditable(true);
			this.weight_increase_textfield.setEditable(true);
			this.weight_decrease_textfield.setEditable(true);
		} else {
			this.weight_textfield.setEditable(false);
			this.weight_increase_textfield.setEditable(false);
			this.weight_decrease_textfield.setEditable(false);
		}
    }
	
	private void sn_panel_dialogwindowClosing(java.awt.event.WindowEvent evt) {
		this.network_panel_checkbox.setSelected(false);
	}
	
	private void sn_graph_dialogwindowClosing(java.awt.event.WindowEvent evt) {
		this.graph_panel_checkbox.setSelected(false);
	}
	
	private void measurements_checkboxitemStateChanged(java.awt.event.ItemEvent evt) {
		this.sn.initMeasurements(	this.measure_connectivity_checkbox.isSelected(),
							 		this.measure_CC_checkbox.isSelected(), 
							 		this.measure_modularity_checkbox.isSelected(),
							 		this.measure_apl_checkbox.isSelected(), 
							 		this.measure_plawC_checkbox.isSelected(),
							 		this.measure_correlationC_checkbox.isSelected());
	}
	
	private void network_panel_checkboxitemStateChanged(java.awt.event.ItemEvent evt) {
		if ( this.network_panel_checkbox.isSelected() ) {
			Point p = this.getLocationOnScreen();
			if ( !this.sn_panel_dialog.isVisible() ) {
				this.sn_panel_dialog.showDialog(p.x + this.getWidth() + 2, p.y - 20);
				this.sn_panel.repaint();
			}
		} else {
			this.sn_panel_dialog.setVisible(false);
		}
	}
	
	private void graph_panel_checkboxitemStateChanged(java.awt.event.ItemEvent evt) {
		if ( this.graph_panel_checkbox.isSelected() ) {
			Point p = this.getLocationOnScreen();
			if ( !this.sn_graph_dialog.isVisible() ) {
				this.sn_graph_dialog.showDialog(p.x, p.y + this.getHeight() + 2);
			}
		} else {
			this.sn_graph_dialog.setVisible(false);
		}
	}
	
	private void network_tabbed_paneStateChanged(javax.swing.event.ChangeEvent evt) {
        JPanel selected_panel = (JPanel)this.network_tabbedpane.getSelectedComponent();

        selected_panel.add(this.num_nodes_label, this.createNewGridBagConstraints(0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));
        selected_panel.add(this.num_nodes_textfield, this.createNewGridBagConstraints(1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL));

        // if this is the import panel... it shows the box but disallows entry of # of agents!

        if (selected_panel == this.import_panel) {
            this.num_nodes_textfield.setEditable(false);
            this.num_nodes_textfield.setText("Press RESET.");
        }

    }

	private void step_buttonActionPerformed(java.awt.event.ActionEvent evt) {
		this.nextTimeStep();
    }
	
    private void play_buttonActionPerformed(java.awt.event.ActionEvent evt) {
    	this.sim_thread.tryPause();
    }

    private void start_buttonActionPerformed(java.awt.event.ActionEvent evt) {
    	this.sim_thread = new SimThread("Thread");
		this.sim_thread.setSpeed( this.speed_combobox.getSelectedIndex() );
		this.sim_thread.start();
    	
		this.toggleButtons(true);
		this.togglePauseButton(false);
	}

    private void reset_buttonActionPerformed(java.awt.event.ActionEvent evt) {
    	this.stopThread();
    	this.resetParameters();
    
		this.sn_panel.cal_nodes_position();
		this.sn_panel.repaint();
		
		this.start_button.setText("Start");
		this.toggleButtons(false);
        this.togglePauseButton(false);
    }

	private void speed_comboboxActionPerformed(java.awt.event.ActionEvent evt) {
		this.sim_thread.setSpeed( this.speed_combobox.getSelectedIndex() );
    }
	
	private void stopThread() {
		if ( this.sim_thread != null ) {
            this.sim_thread.stopThread();
        }
	}

    public void resetParameters() {
        // repopulate GUI values for attrib/var and number of nodes from Pajekfile --
        // important to be done first as SocNetSim2017.Attributes.initAttributes and this.sn.init is dependent on it!
        if (this.network_tabbedpane.getSelectedIndex() == Network.PAJEK)
            this.importPajekGUIValues();


        // BUGFIX: getValue is problematic w/o object error handling!!!!
        // replaced to getText and parseInt or parseDouble as needed


        Attributes.initAttributes(	Integer.parseInt( this.num_attributes_textfield.getText()),
    						Integer.parseInt( this.attr_num_values_textfield.getText()),
    						this.distance_type_combobox.getSelectedIndex(),
    						Double.parseDouble(this.similarity_textfield.getText()) );
		
		Edge.initGlobalEdge( Double.parseDouble(this.weight_textfield.getText()),
                Double.parseDouble(this.weight_increase_textfield.getText()),
                Double.parseDouble(this.weight_decrease_textfield.getText())  );
		
		Agent.initGlobalAgents( Integer.parseInt(this.num_interactions_textfield.getText()),
                Integer.parseInt(this.num_global_interactions_textfield.getText())  );
    	
		this.setInteractionType();

		this.sn.initMeasurements(	this.measure_connectivity_checkbox.isSelected(),
		 		this.measure_CC_checkbox.isSelected(), 
		 		this.measure_modularity_checkbox.isSelected(),
		 		this.measure_apl_checkbox.isSelected(), 
		 		this.measure_plawC_checkbox.isSelected(), true );
		
		this.sn.setAllowGlobalInteraction( this.global_similarity_togglebutton.isSelected() );
		this.sn.setGlobalInteractionFreq( Integer.parseInt(this.global_interactions_freq_textfield.getText()) );
		this.sn.setAllowInfluence( this.influence_togglebutton.isSelected() );
		this.sn.setAllowWeight( this.weight_togglebutton.isSelected() );
		this.sn.setInfluenceMin( Double.parseDouble(this.min_influence_textfield.getText()) );
    	this.sn.setInfluenceMax( Double.parseDouble(this.max_influence_textfield.getText()) );
    	this.sn.setInteractionRadius( Integer.parseInt( this.distance_textfield.getText())  );
        this.sn.init(	Integer.parseInt( this.num_nodes_textfield.getText()) ,
                Double.parseDouble( this.connectivity_textfield.getText()) ,
                Integer.parseInt( this.k_textfield.getText()) ,
                Double.parseDouble( this.shortcut_prob_textfield.getText()) ,
                Integer.parseInt( this.num_branches_textfield.getText()) ,
                this.pajekTextfield.getText()
        );

        if (this.network_tabbedpane.getSelectedIndex() == Network.PAJEK)
            this.sn.importPajekAgents(); // reinit agents from Pajekfile
        else
            this.sn.initAgents();

        // this handles const PAJEK=4 as well...
        //  i.e. if getSelectedIndex() returns const PAJEK
        // reinit 1-based Edges from Pajekfile automatically (to 0-based)
        //  using SocNetSim2017.Network.java's implementation of importPajekEdges
		this.sn.gen_edges(this.network_tabbedpane.getSelectedIndex());

        this.sn.assignInfluenceValue();
		
		this.sn.calcMeasurements();
		this.updatedStatTextarea();
    }
    
    private void setInteractionType() {
    	if ( this.distance_togglebutton.isSelected() && this.similarity_togglebutton.isSelected() ) {
    		this.sn.setInteractionType(SocialNetwork.DISTANCE_SIMILARITY);
    	} else if ( this.distance_togglebutton.isSelected() && !this.similarity_togglebutton.isSelected() ) {
    		this.sn.setInteractionType(SocialNetwork.DISTANCE);
    	} else if ( !this.distance_togglebutton.isSelected() && this.similarity_togglebutton.isSelected() ) {
    		this.sn.setInteractionType(SocialNetwork.SIMILARITY);
    	} else {
    		this.sn.setInteractionType(4);
    	}
    }
	
	private synchronized void nextTimeStep() {
		this.sn.nextTimeStep();
		
		if ( this.sn_panel_dialog.isVisible() ) {
			this.sn_panel.repaint();
		}
		
		if ( this.sn_graph_dialog.isVisible() ) {
			this.updatedStatTextarea();
		}
	}
	
	private synchronized void updatedStatTextarea() {
		String text = "Current timesteps: " + this.sn.getCurrentTimesteps() + "\n";
		this.stat_textarea.setText(text + this.sn.measurementsToString());
	}
	
	private void togglePauseButton(boolean is_pause) {
        if ( this.sim_thread != null ) {
            if ( this.sim_thread.isPause() ) {
                this.play_button.setText("Play  ");
            } else {
                this.play_button.setText("Pause");
            }
        }
    }
    
    private void toggleButtons(boolean running) {
        if ( running ) {
            this.start_button.setEnabled(false);
            this.play_button.setEnabled(true);
            this.step_button.setEnabled(true);
        } else {
            this.start_button.setEnabled(true);
            this.play_button.setEnabled(false);
            this.step_button.setEnabled(false);
        }
    }
	
	class SimThread extends Thread {
        
        private boolean stopped;
        private boolean is_pause;
        private int current_speed;
        
        public SimThread(String str) {
            super(str);
            this.stopped = false;
            this.setSpeed(0);
        }
        
        public void run() {
            
            while ( !this.isStopped() ) {
                try {
                    nextTimeStep();
                    
                    sleep(SPEED[this.getSpeed()]);
                    
                    synchronized(this) {
                        while( this.isPause() ) {
                            wait();
                        }
                    }
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
        
        public void stopThread() {
            this.stopped = true;
        }
        
        private boolean isStopped() {
            return this.stopped;
        }
        
         /**
         * Pause or unpause the simulation.
         */    
        public synchronized void tryPause() {
            if ( !this.isPause() ) {
                notifyAll();
                this.setPause(true);
            } else {
                notifyAll();
                this.setPause(false);
            }
        }

        /**
         * Set the pause status of the simulation.
         * @param pause the pause status.
         */    
        private void setPause(boolean pause) {
            this.is_pause = pause;
        }

        /**
         * Test if the simulation is paused.
         * @return <CODE>True</CODE> if the simulation is paused. <CODE>False</CODE> otherwise.
         */    
        private boolean isPause() {
            return this.is_pause;
        }
        
        public void setSpeed(int speed) {
            if (speed >= 0 && speed < SPEED.length) {
                this.current_speed = speed;
            } else {
                this.current_speed = SPEED.length - 1;
            }
        }

        public int getSpeed() {
            return this.current_speed;
        }
            
    }
	
	class DialogBox extends JDialog {
		
		public DialogBox(JFrame parent, JComponent panel, String title ) {
			JScrollPane jsp = new JScrollPane( panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
			getContentPane().add("Center", jsp);
			
			setTitle(title);

			pack();
		}

		public void showDialog(int x, int y) {
			setLocation(x, y);
			pack();
			setVisible(true);
      	}
	}
	
	private JButton createNewButton(String title) {
		JButton new_button = new JButton();
		new_button.setText(title);
		new_button.setFont(GUIControlPanel.FONT);
		// new_button.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
		
		return new_button;
	}
	
	private JToggleButton createNewToggle(String title, boolean selected) {
		JToggleButton new_button = new JToggleButton(title, selected);
		new_button.setFont(GUIControlPanel.FONT);
		
		return new_button;
	}
	
	private JFormattedTextField createNewTextfield(NumberFormatter formatter, int size, Object value) {
		JFormattedTextField new_textfield = new JFormattedTextField(formatter);
		new_textfield.setFont(GUIControlPanel.FONT);
		new_textfield.setColumns(size);
		new_textfield.setValue(value);
		new_textfield.setHorizontalAlignment(JFormattedTextField.CENTER);
		
		return new_textfield;
	}
	
	private JLabel createNewLabel(String title, Component label_for) {
		JLabel new_label = new JLabel();
		new_label.setFont(GUIControlPanel.FONT);
		new_label.setText(title);
		new_label.setLabelFor(label_for);
		
		return new_label;
	}
	
	private GridBagConstraints createNewGridBagConstraints(int x, int y, int position, int fill) {
		return new GridBagConstraints(x, y, 1, 1, 0.0, 0.0, position, fill, new Insets(0, 0, 0, 0), 0, 0);
	}
	
	private GridBagConstraints createNewGridBagConstraints(int x, int y, int grididwith, int gridheight, int position, int fill) {
		return new GridBagConstraints(x, y, grididwith, gridheight, 0.0, 0.0, position, fill, new Insets(0, 0, 0, 0), 0, 0);
	}
	
	private NumberFormatter createNewNumberFormatter(Comparable mininum, Comparable maximum, int min_digit, int max_digit) {
		NumberFormat format = NumberFormat.getNumberInstance();
		format.setMinimumFractionDigits(min_digit);
		format.setMaximumFractionDigits(max_digit);
		NumberFormatter formatter = new NumberFormatter(format);
        formatter.setMinimum(mininum);
        formatter.setMaximum(maximum);
        
        return formatter;
	}
	
	private NumberFormatter createNewNumberFormatter(Comparable mininum, Comparable maximum) {
		NumberFormat format = NumberFormat.getNumberInstance();
		NumberFormatter formatter = new NumberFormatter(format);
        formatter.setMinimum(mininum);
        formatter.setMaximum(maximum);
        
        return formatter;
	}
	
	private JCheckBox createNewCheckbox(String title, boolean selected) {
		JCheckBox new_checkbox = new JCheckBox();
		new_checkbox.setText(title);
		new_checkbox.setSelected(selected);
		new_checkbox.setFont(GUIControlPanel.FONT);
		
		return new_checkbox;
	}
	
	private JTextArea createNewTextarea(int rows, int columns, String text, boolean linewrap, boolean editable) {
		JTextArea new_textarea = new JTextArea(rows, columns);
		new_textarea.setText(text);
		new_textarea.setFont(GUIControlPanel.FONT);
		new_textarea.setLineWrap(linewrap);
		new_textarea.setEditable(editable);
		
		return new_textarea;
	}

	private void importPajekGUIValues() {
	    String pajek_filename = pajekTextfield.getText();
        try {
            BufferedReader inFile = new BufferedReader(new FileReader(pajek_filename));
            System.out.println("Opening " + pajek_filename + " for nodes...");

            String line = null;
            //boolean startReading = false;
            //int totalAgents = 0;
            while ((line = inFile.readLine()) != null) {

                if (line.startsWith("c statesopts")) {
                    StringTokenizer tok = new StringTokenizer(line, " ");
                    try {
                        tok.nextToken(); // skip "c"
                        tok.nextToken(); // skip "statesopts"
                        tok.nextToken(); // skip "attno"
                        String attno = tok.nextToken();
                        tok.nextToken(); // skip "valno"
                        String valno = tok.nextToken();

                        this.num_attributes_textfield.setText(attno);
                        this.attr_num_values_textfield.setText(valno);
                        //SocialNetworkApplication.application().getSettingsWindow().getAgentPanel().getNumAttributesTextfield().setValue(Double.valueOf(attno));
                        //SocialNetworkApplication.application().getSettingsWindow().getAgentPanel().getNumValuesTextfield().setValue(Double.valueOf(valno));
                        //System.out.println("set: attno=" + attno + " valno=" + valno);
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }

                StringTokenizer st = new StringTokenizer(line);
                String header = st.nextToken();


                if (header.equalsIgnoreCase("*Vertices")) {
                    //startReading = true;
                    //CARGroup agentSettings = SocialNetworkApplication.application().getSettingType(CDM.AgentSettings.Name);
                    //agentSettings.setSetting(CDM.AgentSettings.NumAgents, st.nextToken());
                    //totalAgents = Integer.parseInt( st.nextToken() );

                    this.num_nodes_textfield.setText(st.nextToken() );
                    continue;
                }


                if (header.equalsIgnoreCase("*Edges")) {
                    //startReading = false;
                    break;
                }

            // THAT IS ALL FOR POPULATING ATTRIBUTES AND NUM VALUES; AS WELL AS READ TOTAL AGENTS (NODES) REQUIRED FOR GRAPH INIT
            }
        } catch (IOException iox) {
            System.out.println("Problem opening " + pajek_filename);
        }
    }
}
