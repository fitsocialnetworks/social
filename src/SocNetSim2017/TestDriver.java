package SocNetSim2017;

/**
 * DRIVER/ENGINE
 * DEPRECATED
 * SocNetSim2017.TestDriver
 * has main to run arbitrary test code
 **/

public class TestDriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Attributes.initAttributes(5, 2, Attributes.HAMMING_DISTANCE, 0);
		/*
		SocNetSim2017.SocialNetwork network = new SocNetSim2017.SocialNetwork(5);
		network.setInteractionRadius(20);
		network.initAttributes(1000, 0.1, 2, 0.01, 2);
		network.initAgents();
		System.out.println("Init agents");
		network.gen_edges(SocNetSim2017.Network.RANDOM_GRAPH);
		System.out.println("Gen edges");
		*/
		/*
		Hashtable vertices = network.get_vertices();
		for ( int i = 0; i < network.size(); i++ ) {
			Agent a = (Agent) vertices.get( Integer.valueOf(i) );
			System.out.println(a);
		}
		*/
		// network.neighbours_interaction();
		// network.full_interaction();
		// network.get_agent_radius((Agent)network.get_vertex(0), 6);
		// network.radius_interaction();
		//network.interaction();
		
		/*
		SocNetSim2017.Attributes a = new SocNetSim2017.Attributes();
		a.setRandomValues();
		
		SocNetSim2017.Attributes b = new SocNetSim2017.Attributes();
		b.setRandomValues();
		
		System.out.println( a + " : " + b );
		System.out.println( SocNetSim2017.Attributes.hammingDistance(a, b) );
		System.out.println( SocNetSim2017.Attributes.manhattanDistance(a, b) );
		System.out.println( SocNetSim2017.Attributes.euclideanDistance(a, b) );
		*/
		
		Agent a = new Agent(0);
		a.attributes.setRandomValues();
		System.out.println(a);
		Agent b = new Agent(1);
		b.attributes.setRandomValues();
		System.out.println(b);
		
		System.out.println( "Is similar: " + a.is_similar(b) );
		//a.interact(b);
		
		
		Integer one = new Integer(1);
		Integer one1 = new Integer(1);
		Integer two = new Integer(2);
		
		System.out.println( one.equals(one1) );
		System.out.println( one.equals(two) );
		System.out.println( Integer.numberOfLeadingZeros(10) );
		
		for ( int i = 0; i <= 10; i++ ) {
			System.out.println( i % 5 );
		}
		
		
		System.out.println("_----------------------");
		int n = 100;
		double[] x = new double[n];
		double[] y = new double[n];
		for ( int i = 1; i <= n; i++ ) {
			x[i-1] = Math.log(i);
			y[i-1] = Math.log( Math.pow(i, -2.5) );
			// System.out.println( i + " " + Math.pow(i, -2.5) + " " + "Log(" + i + "): " + Math.log(i));
		}
		double m = TestDriver.m(x, y);
		double bb = TestDriver.b(x, y, m);
		System.out.println( "m: " + m + " b: " + bb );
		// System.out.println( ((Math.log(0.177) - Math.log(1))/Math.log(2)) );
	}
	
	public static double m(double[] x, double[] y) {
		double sum_xy = 0,
			   sum_x = 0,
			   sum_y = 0,
			   sum_x2 = 0;
		
		for ( int i = 0; i < x.length; i++ ) {
			sum_xy += (x[i] * y[i]);
			sum_x += x[i];
			sum_y += y[i];
			sum_x2 += Math.pow(x[i], 2);
		}
		
		return ( x.length * sum_xy - sum_x * sum_y ) / ( x.length * sum_x2 - sum_x * sum_x );
	}
	
	public static double b(double[] x, double[] y, double m) {
		double sum_x = 0,
		   	   sum_y = 0;
		
		for ( int i = 0; i < x.length; i++ ) {
			sum_x += x[i];
			sum_y += y[i];
		}
		   	      	   
		return ( sum_y - m * sum_x ) / x.length;
	}
}