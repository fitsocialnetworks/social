package SocNetSim2017; /**
 * DATA STRUCTURE
 * SocNetSim2017.Attributes.java
 *
 * Created on 23 January 2007
 *
 * @author  Alex Tee Neng Heng
 **/

import java.util.*;
import java.io.*;

public class Attributes extends ArrayList<Integer> implements Serializable {
	private static final long serialVersionUID = 8683452581122892189L;
	private static Random RANDOM = new Random();
	public static int NUM_ATTRIBUTES = 1;
	public static int NUM_VALUES = 2;
	public static int[] VALUES;
	public static int COMPARE_TYPE = 0;
	public static final int HAMMING_DISTANCE = 0;
	public static final int MANHATTAN_DISTANCE = 1;
	public static final int EUCLIDEAN_DISTANCE = 2;
	public static double[] MAX_DISTANCE;
	
	public static double SIMILARITY = 100;
	public static double GLOBAL_SIMILARITY = 100;
	
	public Attributes() {
		super( Attributes.NUM_ATTRIBUTES );
	}
	
	public static double getDistance(Attributes a, Attributes b) {
		switch ( Attributes.COMPARE_TYPE ) {
			case 0:
				return Attributes.hammingDistance(a, b);
				
			case 1:
				return Attributes.manhattanDistance(a, b);
				
			case 2:
				return Attributes.euclideanDistance(a, b);
				
			default:
				return Attributes.hammingDistance(a, b);
		}
	}
	
	public static void initAttributes(int num_attrs, int num_values, int compare_type, double similarity) {
		Attributes.NUM_ATTRIBUTES = num_attrs;
		Attributes.NUM_VALUES = num_values;
		Attributes.COMPARE_TYPE = compare_type;
		Attributes.SIMILARITY = similarity;
		
		Attributes a = new Attributes();
		Attributes b = new Attributes();
		for ( int i = 0; i < num_attrs; i++ ) {
			a.add( new Integer(0) );
			b.add( new Integer(num_values-1) );
		}
		
		Attributes.MAX_DISTANCE = new double[3];
		Attributes.MAX_DISTANCE[Attributes.HAMMING_DISTANCE] = num_attrs;
		Attributes.MAX_DISTANCE[Attributes.MANHATTAN_DISTANCE] = Attributes.manhattanDistance(a, b);
		Attributes.MAX_DISTANCE[Attributes.EUCLIDEAN_DISTANCE] = Attributes.euclideanDistance(a, b);

		a = b = null;
	}
	
	public static int hammingDistance(Attributes a, Attributes b) {
		int distance = 0;
		for ( int i = 0; i < a.size(); i++ ) {
			if ( ((Integer)a.get(i)).intValue() != ((Integer)b.get(i)).intValue() ) {
				distance++;
			}
		}

		return distance;
	}

	public static double manhattanDistance(Attributes a, Attributes b) {
		double distance = 0;
		for ( int i = 0; i < a.size(); i++ ) {
			distance += Math.abs( ((Integer)a.get(i)).intValue() - ((Integer)b.get(i)).intValue() );
		}

		return distance;
	}

	public static double euclideanDistance(Attributes a, Attributes b) {
		double distance = 0;
		for ( int i = 0; i < a.size(); i++ ) {
			distance += Math.pow( ((Integer)a.get(i)).intValue() - ((Integer)b.get(i)).intValue(), 2 );
		}

		return Math.sqrt( distance );
	}
	
	public void setRandomValues() {
		super.clear();
		for ( int i = 0; i < Attributes.NUM_ATTRIBUTES; i++ ) {
			super.add( new Integer( Attributes.RANDOM.nextInt( Attributes.NUM_VALUES ) ) );
		}
	}

	public void setZeroValues() {
        // based on code in SocNetSim2017.Attributes.java - fromString
        // using a default of: newAgent.getAttributes().fromString("\"\"");

        // i.e. everything is zero anyway as add(0); will trigger for NUM_ATTRIBUTES
        // see setStringAttributeValues for implementation

		super.clear();
		for ( int i = 0; i < Attributes.NUM_ATTRIBUTES; i++ ) {
			super.add( new Integer( 0 ));
		}

	}

	public void setStringAttributeValues(String s) {
	    // based on code in SocNetSim2017.Attributes.java - fromString
        // using a passed string of: newAgent.getAttributes().fromString( st.nextToken() );

        super.clear();
        if (s.startsWith("\"") && s.endsWith("\""))
            s = s.substring(1, s.length() - 1);

        clear();
        StringTokenizer tok = new StringTokenizer(s, "-");
        while (tok.hasMoreTokens() && size() < Attributes.NUM_ATTRIBUTES) {
            super.add(Integer.parseInt(tok.nextToken()));
        }

        while (size() < NUM_ATTRIBUTES)
            super.add(0);

    }
	
	public String toString() {
		String text = "";
		for ( int i = 0; i < super.size(); i++ ) {
			text += ((Integer)super.get(i)).intValue();
		}
		
		return text;
	}
	public String toStringWithDashes() {
		String text = "";
		for ( int i = 0; i < super.size(); i++ ) {
			text += ((Integer)super.get(i)).intValue();

            if (i != super.size() - 1) // if we're NOT dealing with the last item
                text += "-";
		}

		return text;
	}
	
	public static double calcSimilarity(Attributes a, Attributes b) {
		return (100 - Attributes.getDistance(a, b)/Attributes.MAX_DISTANCE[Attributes.COMPARE_TYPE] * 100.0);
	}
	
	public static void setSimilarity(double s) {
		if ( s >= 0 && s <= 100 ) {
			Attributes.SIMILARITY = s;
		} else {
			Attributes.SIMILARITY = 10;
		}
	}
	
	public static int getRandomAttributeIndex() {
		return Attributes.RANDOM.nextInt(NUM_ATTRIBUTES);
	}
}
