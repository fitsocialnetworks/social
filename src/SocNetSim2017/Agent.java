package SocNetSim2017;
/*
 * Agent.java
 *
 * Created on 23 January 2007
 */

/**
 *
 * @author  Alex Tee Neng Heng
 */

import java.awt.*;
import java.util.Random;


public class Agent extends Vertex {
	public static SocialNetwork MY_SN;
	private static Random RANDOM = new Random();
	public static final int RADIUS = 3;
	public static final int DIAMETER = 6;
	public static int COUNT = 0;
	public static int INTERACTIONS_PER_TIMESTEP = 1;
	public static int GLOBAL_INTERACTIONS_PER_TIMESTEP = 1;

	// public Point location;
	public Attributes attributes;
	public Point location;
	private double influence;

	public Agent(int index) {
		super(index);
		this.location = new Point();
		this.attributes = new Attributes();
	}
	
	public void set_influence(double i) {
		this.influence = i;
	}
	
	public double get_influence() {
		return this.influence;
	}

	public void set_location(int x, int y) {
		this.location.x = x;
		this.location.y = y;
	}
	
	public String toString() {
		String text = super.toString();
		text += " Attributes: " + this.attributes.toString();
		
		return text;
	}
	
	public synchronized void interact(Agent a) {
		if ( Agent.MY_SN.getAllowInfluence() ) {
			this.influence(a);
		}
		if ( Agent.MY_SN.getAllowWeight() ) {
			this.maintain_link(a);
		}
	}
	
	private synchronized void influence(Agent a) {
		int index = Attributes.getRandomAttributeIndex();
		
		if ( !this.attributes.get(index).equals(a.attributes.get(index)) 
			 && Agent.RANDOM.nextDouble() < this.influence ) {
			a.attributes.set(index, Integer.valueOf( this.attributes.get(index).intValue() ));
			// System.out.println("Influence");
		}
	}
	
	private synchronized void maintain_link(Agent a) {
		if ( Agent.MY_SN.edges.containsKey("" + super.getIndex() + a.getIndex()) ) {
			Agent.MY_SN.edges.get( "" + super.getIndex() + a.getIndex() ).increaseWeight();
			// System.out.println("HA HA HA1");
		} else if ( Agent.MY_SN.edges.containsKey("" + a.getIndex() + super.getIndex()) ) {
			Agent.MY_SN.edges.get( "" + a.getIndex() + super.getIndex() ).increaseWeight();
			// System.out.println("HA HA HA2");
		} else {
			Agent.MY_SN.make_new_edge(super.getIndex(), a.getIndex());
			// System.out.println("HO HO HO  " + super.getIndex() + " " + a.getIndex());
		}
	}
	
	public boolean is_similar(Agent a) {
		return Attributes.calcSimilarity(this.attributes, a.attributes) >= Attributes.SIMILARITY;
	}
	
	public static void initGlobalAgents(int num_interactions, int num_global_interactions) {
		Agent.INTERACTIONS_PER_TIMESTEP = num_interactions;
		Agent.GLOBAL_INTERACTIONS_PER_TIMESTEP = num_global_interactions;
	}
}
