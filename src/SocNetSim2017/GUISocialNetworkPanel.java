package SocNetSim2017;

import java.awt.*;
import java.util.*;
import java.awt.geom.Ellipse2D;
import javax.swing.*;
import java.awt.event.*;


public class GUISocialNetworkPanel extends JPanel implements MouseListener {
	
	private Dimension size;
	private SocialNetwork sn;
	private Ellipse2D.Double oval = new Ellipse2D.Double();
	
	public GUISocialNetworkPanel(SocialNetwork sn, int width, int height) {
		this.sn = sn;
    	this.oval.height = Agent.DIAMETER;
    	this.oval.width = Agent.DIAMETER;
		this.initGUI(width, height);
		// addMouseListener(this);

	}
	
	private void initGUI(int width, int height) {
		this.size = new Dimension(width, height);
		setPreferredSize( this.size );
        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0)));
	}
	
	protected synchronized void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		this.paint_edges(g);
		this.paint_nodes(g);
		
		g.dispose();
	}
	
	private synchronized void paint_nodes(Graphics g) {
		g.setColor(Color.BLUE);
		
		for ( Enumeration e = this.sn.vertices.elements(); e.hasMoreElements(); ) {
			Agent agent = (Agent) e.nextElement();
			g.fillOval(agent.location.x - Agent.RADIUS, agent.location.y - Agent.RADIUS, Agent.DIAMETER, Agent.DIAMETER);
		}
		
		/*
		g.setColor(Color.RED);
		for ( int i = 0; i < this.sn.interact_agents.size(); i++ ) {
			Agent agent = (Agent) this.sn.interact_agents.get(i);
			g.fillOval(agent.location.x - Agent.RADIUS, agent.location.y - Agent.RADIUS, Agent.DIAMETER, Agent.DIAMETER);
		}
		
		g.setColor(Color.YELLOW);
		g.fillOval(this.sn.picked_agent.location.x - Agent.RADIUS, this.sn.picked_agent.location.y - Agent.RADIUS, Agent.DIAMETER, Agent.DIAMETER);
		*/
	}
	
	private synchronized void paint_edges(Graphics g) {
		if (sn.isTimeStepCurrentlyGlobal())
            g.setColor(Color.RED);
        else
            g.setColor(Color.GRAY);
		
		for ( Enumeration e = this.sn.edges.elements(); e.hasMoreElements(); ) {
			Edge edge = (Edge) e.nextElement();
			g.drawLine(
                    (int) ((Agent)edge.p1).location.x ,
                    (int) ((Agent)edge.p1).location.y ,
                    (int) ((Agent)edge.p2).location.x ,
                    (int) ((Agent)edge.p2).location.y 
                   );
		}
	}

	/**
     * Calculate the position of the nodes in the panel. The nodes are placed in a
     * circle.
     */    
    public void cal_nodes_position() {
      
        if ( this.sn != null ) {
            Enumeration vertices = this.sn.get_vertices().elements();
            Agent a = null;

            int[] num_nodes = {300, 250, 200, 150, 100};
            int[] radius = {340, 305, 265, 225, 185};

            int remain = this.sn.get_vertices().size();
            int remain_size;
            for ( remain_size = 0; remain_size < num_nodes.length; remain_size++ ) {
                if ( remain > num_nodes[remain_size] ) {
                    remain -= num_nodes[remain_size];
                } else {
                    break;
                }
            }

            int center_x = this.size.width / 2,
                center_y = this.size.height / 2;

            double theta, x_1, y_1, x_i, y_i, theta_i;   

            theta = x_1 = y_1 = theta_i = 0;
            for ( int i = 0; i < num_nodes.length; i++ ) {
                for ( int j = 0; j < num_nodes[i] && vertices.hasMoreElements(); j++ ) {
                    a = (Agent) vertices.nextElement();

                    if ( j > 0 ) {
                        theta_i += theta;
                        x_i = ( Math.cos(theta_i) * x_1 ) + ( Math.sin(theta_i) * y_1 ) + center_x;
                        y_i = ( Math.cos(theta_i) * y_1 ) - ( Math.sin(theta_i) * x_1 ) + center_y;
                    } else {
                        if ( i == remain_size ) {
                            theta = 2 * Math.PI / remain;
                        } else {
                            theta = 2 * Math.PI / num_nodes[i];
                        }
                        x_1 = 0;
                        y_1 = radius[i];
                        theta_i = 0;

                        x_i = 0 + center_x;
                        y_i = y_1 + center_y;
                    }
					a.location.x = (int)x_i;
					a.location.y = (int)y_i;
                }
            }
        }
    }
    
    
    public void mouseClicked(MouseEvent e){
    	for ( int i = 0; i <  this.sn.size(); i++ ) {
    		Agent a = (Agent) this.sn.get_vertex(i);
    		this.oval.x = a.location.x - Agent.RADIUS;
    		this.oval.y = a.location.y - Agent.RADIUS;
    		if ( this.oval.contains(e.getX(), e.getY()) ) {
    			System.out.println(a);
    			break;
    		}
    	}
    }
    
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    /*
    public String getToolTipText(MouseEvent evt) {
        int x = evt.getX();
        int y = evt.getY();
        
        for ( int i = 0; i <  this.sn.size(); i++ ) {
    		Agent a = (Agent) this.sn.get_vertex(i);
    		this.oval.x = a.location.x - Agent.RADIUS;
    		this.oval.y = a.location.y - Agent.RADIUS;
    		if ( this.oval.contains(x, y) ) {
    			System.out.println("aaa");
    			return a.toString();
    		}
    	}
        System.out.println("bbb");    
        return "";
    }
    */
}
