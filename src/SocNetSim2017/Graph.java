package SocNetSim2017;/*
 * SocNetSim2017.Network.java
 *
 * Created on 13 October 2005
 */

/**
 *
 * @author  Alex Tee Neng Heng
 */

import java.util.*;

public class Graph {
	protected Edge[][] matrix;
	protected Hashtable<Integer, Vertex> vertices;
	protected Hashtable<String, Edge> edges;

	public Graph(int size) {
		this.vertices = new Hashtable<Integer, Vertex>();
		this.edges = new Hashtable<String, Edge>();
		this.init(size);
	}

	public void init(int size) {

		this.vertices.clear();
		this.edges.clear();
		
		this.matrix = new Edge[size][size];

		for ( int row = 0; row < size; row++ ) {
			for ( int column = 0; column < size; column++ ) {
				this.matrix[row][column] = null;
			}
		}
                
	}

	public int size() {
		return this.matrix.length;
	}

	public Hashtable get_vertices() {
		return this.vertices;
	}

	public Hashtable get_all_edges() {
		return this.edges;
	}

	public void set_vertex(int index, Vertex v) {
		this.vertices.put( Integer.valueOf( "" + index ), v );
	}

	public Vertex get_vertex(int index) {
		return (Vertex)this.vertices.get( Integer.valueOf( "" + index ) );
	}
	
	public boolean set_edge(String key, Edge e) {
		this.edges.put(key, e);
		return true;
	}

	public boolean set_edge(Edge e) {
		if ( e != null ) {
			this.edges.put( e.getIDString(), e );
			return true;
		}
		return false;
	}
	
	public Edge delete_edge(String key) {
		return this.edges.remove(key);
	}

	public Edge delete_edge(Edge e) {
		if ( e == null ) {
			System.out.println("oh no");
		
		}
		return (Edge) this.edges.remove( e.getIDString() );
	}
	
	public boolean set_edge(int row, int column, Edge edge) {
		try {
			this.matrix[row][column] = edge;
			
			return true;
		} catch ( IndexOutOfBoundsException e ) {
			return false;
		}
	}
        
    public Edge get_edge(int row, int column) {
        try {
			return this.matrix[row][column];
		} catch ( IndexOutOfBoundsException e ) {
			return null;
		}
    }

	public Edge delete_edge(int row, int column) {
		try {
			Edge removed_edge = this.matrix[row][column];
			this.matrix[row][column] = null;

			return removed_edge;
		} catch ( IndexOutOfBoundsException e ) {
			return null;
		}
	}
	
	public boolean exist(int row, int column) {
		try {
			if ( this.matrix[row][column] != null ) {
				return true;
			} else {
				return false;
			}
		} catch ( IndexOutOfBoundsException e ) {
			return true;
		}
	}
}
