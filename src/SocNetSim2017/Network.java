package SocNetSim2017;/*
 * SocNetSim2017.Network.java
 *
 * Created on 13 October 2005
 */

/**
 *
 * @author  Alex Tee Neng Heng
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * SocNetSim2017.Network - Basic premise of a network
 * Has utility classes
 */
public class Network extends Graph {
	/**
     * Identifies the random network.
     */    
    public static final int RANDOM_GRAPH = 0;
    /**
     * Identifies the small world network.
     */    
    public static final int SMALL_WORLD = 1;
    /**
     * Identifies the scale free network.
     */    
    public static final int SCALE_FREE = 2;
    /**
     * Identifies the tree sttuctured network.
     */    
    public static final int TREE = 3;

    public static final int PAJEK = 4;

	protected Random random;

	private double connectivity;
    private int k;              // nearest neighbours, k, for small-world network
    private double shortcuts_prob;  // shortcuts probability
    private int num_leafs;
	private String pajek_filename; // for PAJEK

	public Network(int size) {
		super(size);
		this.random = new Random();
	}

	// INITIALIZER MODIFIED TO HAVE 6 PARAMS - subclass initializer must adhere as well.
    // this calls SocNetSim2017.Graph.java (Superclass) init which clears V,E and inits the matrix
	public void init(int size, double conn, int k, double s_prob, int num_l, String newPajekFilename) {
		super.init(size);
		this.set_connectivity( conn );
		this.set_k( k );
		this.set_shortcuts_probability( s_prob );
		this.set_num_leaf( num_l );
        this.setPajek_filename( newPajekFilename );
	}

	/*
	public void init_network(double conn, int k, double s_prob, int num_l) {
		this.set_connectivity( conn );
		this.set_k( k );
		this.set_shortcuts_probability( s_prob );
		this.set_num_leaf( num_l );
	}
	*/

	/*
	public void gen_vertices() {
		for (int i = 0; i < super.size(); i++) {
			super.set_vertex( i, new SocNetSim2017.Vertex(i) );
            }
	}
    */

	public void gen_edges(int graph_type) {
        switch (graph_type) {
            case 0:     // Random graph
                this.random_graph();
                break;
                
            case 1:     
            	this.small_world();
                break;
                
            case 2:     
                
                this.scale_free();
                break;
                
            case 3:
                this.tree();
                break;

            // special case: all from pajek.
            case PAJEK:
                this.importPajekEdges();
                break;

            default:
                this.random_graph();
                break;
        }
    }

	/**
     * Change the SocNetSim2017.Network with the specified connectivity.
     * @param connectivity the specified connectivity.
     */    
    public void set_connectivity(double connectivity) {
        if ( connectivity >=0 && connectivity <= 1.0 ) {
            this.connectivity = connectivity;
        } else {
            this.connectivity = 0.5;
        }
    }
    
    /**
     * Return the connectivity of the SocNetSim2017.Network.
     * @return the connectivity of the SocNetSim2017.Network.
     */    
    public double get_connectivity() {
        return this.connectivity;
    }

	/**
     * Calculate the number of edges in the random network according the connectivity.
     * Then generates the random edges.
     */    
    public void random_graph() {
        int max_edges = ( super.size() * ( super.size() - 1 ) ) / 2;
        double num_edges = max_edges * this.get_connectivity();
        this.gen_random_world( (int)num_edges );
    }

/**
     * Set the range of the neighbours for each node of the small world network.
     * @param k the range of the neighbours
     */    
    public void set_k(int k) {
        if ( k >= 0 ) {
            if ( k > super.size() / 2 ) {
                this.k = super.size() / 2;
            } else {
                this.k = k;
            }
        } else {
            this.k = 2;
        }
    }
    
    /**
     * Return the range of the neighbours (for small worl network).
     * @return the range of the neighbours.
     */    
    public int get_k() {
        return this.k;
    }
    
    /**
     * Set the probability of the shortcuts per link.
     * @param short_prob the probability of the shortcuts per link.
     */    
    public void set_shortcuts_probability(double short_prob) {
        if ( short_prob >= 0 && short_prob <= 1.0 ) {
            this.shortcuts_prob = short_prob;
        } else {
            this.shortcuts_prob = 0.1;
        }
    }
    
    /**
     * Return the probability of the shortcuts per link.
     * @return the probability of the shortcuts per link.
     */    
    public double get_shortcuts_probability() {
        return this.shortcuts_prob;
    }
    
    /**
     * Generates a small worl network.
     */    
    public void small_world() {
        int origin, target;
		int size = super.size();
        
        for (int i = 0; i < size; i++) {
            for (int j = 1; j <= this.get_k(); j++) {
                origin = i; target = (i+j) % size;
                if ( !super.exist( origin, target ) ) {
                    this.make_new_edge(origin, target);
                }
            }
        }
        
        // int max_edges = ( size * ( size - 1 ) ) / 2;
        int num_shortcuts = (int) ( this.get_shortcuts_probability() * this.get_k() * size );
        
        if ( this.get_k() < size / 2 - (size/10) ) {
            this.gen_random_world(num_shortcuts);
        }
    }
    
    public void scale_free() {
    	int[] edge_freq = new int[super.size()];
    	Arrays.fill(edge_freq, 0);
    	this.make_new_edge(0, 1);
    	edge_freq[0] += 50;
    	edge_freq[1] += 10;
    	int num_edges = 60;
    	
    	//System.out.println("dddf");
    	
    	for ( int i = 2; i < super.size(); i++ ) {
    		int rand_num = this.random.nextInt(num_edges);
    		int sum = 0;
    		for ( int j = 0; j < edge_freq.length; j++ ) {
    			sum += edge_freq[j];
    			
    			if ( rand_num < sum ) {
                    this.make_new_edge( i, j );
                    edge_freq[i] += 50;
                    edge_freq[j] += 10;
    				num_edges += 60;
    				break;
    			}
    		}
    	}
    	//System.out.println("dddf");
    	/*
    	int[] edge_distribution = new int[super.size()];
		Arrays.fill(edge_distribution, 0);
		for (int i = 0; i < super.size(); i++) {
			edge_distribution[super.get_vertex(i).neighbours.size()]++;
		}

		System.out.println("-----------------------------------");
		for ( int i = 0; i < edge_distribution.length; i++ ) {
			if ( edge_distribution[i] > 0 ) {
				System.out.println( i + "\t" + edge_distribution[i] );
			}
		}
		System.out.println("-----------------------------------");
		*/
    }
    
    /**
     * Generates a scale free network.
     */    
    public void scale_free1() {
		ArrayList<Vertex> edge_frequency = new ArrayList<Vertex>();
		int origin = 0, 
            target = 0, 
            length = 2,
			size = super.size(),
            rand_num, sum;
        this.make_new_edge(0, 1);
        edge_frequency.add( super.get_vertex(0) );
        edge_frequency.add( super.get_vertex(1) );
		
		length = edge_frequency.size();
		for (int i = 2; i < size; i++) {
            length = edge_frequency.size();
			this.sort( edge_frequency );
            // rand_num = this.random.nextInt( ( length * ( length + 1 ) ) / 2 );
			// rand_num = this.random.nextInt( (int)( Math.pow(length, 2) * Math.pow(length + 1, 2) / 4 ) );   // k^3 sum
			rand_num = this.random.nextInt( (int)( Math.pow(length, 2) * Math.pow(length + 1, 2) / 4 ) );
            sum = 0;
            
            for (int j = 0; j < length; j++) {
                // sum += j+1;
				sum += (int) Math.pow(j+1, 3);
				// sum += (int) Math.pow(j+1, 3);
				// sum += ((SocNetSim2017.Vertex)edge_frequency.get(j)).get_neighbours().size();
                if ( rand_num < sum ) {
                    origin = i;
                    target = j;
                    this.make_new_edge( origin, target );
                    break;    
                }
            }
            
            edge_frequency.add( super.get_vertex(i) );
        }

		/*
		int[] edge_distribution = new int[super.size()];
		Arrays.fill(edge_distribution, 0);
		for (int i = 0; i < super.size(); i++) {
			edge_distribution[super.get_vertex(i).neighbours.size()]++;
		}

		System.out.println("-----------------------------------");
		for ( int i = 0; i < edge_distribution.length; i++ ) {
			System.out.println( i + "\t" + edge_distribution[i] );
		}
		System.out.println("-----------------------------------");
		*/
    }

	protected void sort(ArrayList<Vertex> nodes) {
		int min;
        Vertex temp;
        
        for (int i = 0; i < nodes.size()-1; i++) {
            min = i;
            for (int j = i+1; j < nodes.size(); j++) {
				if ( ((Vertex)nodes.get(j)).neighbours.size() < ((Vertex)nodes.get(min)).neighbours.size() ) {
                // if ( ((SocNetSim2017.Vertex)nodes.get(j)).get_neighbours().size() < ((SocNetSim2017.Vertex)nodes.get(min)).get_neighbours().size() ) {
                    min = j;
                }
            }
            temp = ((Vertex)nodes.get(i));
            nodes.set( i, nodes.get(min) );
            nodes.set( min, temp );
        }
	}

	/**
     * Set the number of children of each node of the tree network.
     * @param num_leafs the number of children of each node.
     */    
    public void set_num_leaf(int num_leafs) {
        if ( num_leafs > 1 && num_leafs <= super.size() - 1 ) {
            this.num_leafs = num_leafs;
        } else {
            this.num_leafs = 2;
        }
    }
    
    /**
     * Return the number of children of each node.
     * @return the number of children of each node.
     */    
    public int get_num_leafs() {
        return this.num_leafs;
    }

	/**
     * Generates a tree network.
     */    
    public void tree() {
        int parent_pointer = 0, 
            empty_pointer = 1;
        int leafs = this.get_num_leafs();
		int size = super.size();
			
        while ( empty_pointer < size ) {
            for ( int i = 0; i < leafs && empty_pointer < size; i++ ) {
				this.make_new_edge(parent_pointer, empty_pointer);
				empty_pointer++;
            }
            parent_pointer++;
        }
    }
    
    /**
     * Generates a random network with the specified number of edges.
     * @param num_edges the number of edges in the random network.
     */    
    public void gen_random_world(int num_edges) {
        boolean added;
        int origin, target;
		int size = super.size();
        
        for (int i = 0; i < (int) num_edges; i++) {
            added = false;
            while ( !added ) {
                origin = this.random.nextInt( size );
                target = this.random.nextInt( size );
                
                if ( origin != target && !super.exist( origin, target ) && !super.exist( target, origin ) ) {
					this.make_new_edge(origin, target);
                    added = true;
                }
                
            }
        }
    }
    
    public Edge make_new_edge_directed(int origin, int target) {
    	Edge new_edge = new Edge( super.get_vertex( origin ), super.get_vertex( target ) );
    	
    	super.set_edge("" + origin + target, new_edge);
		super.get_vertex(origin).neighbours.put( Integer.valueOf( super.get_vertex( target ).getIndex() ), super.get_vertex( target ) );
    	
    	return new_edge;
    }
    
    public Edge make_new_edge_undirected(int p1, int p2) {
    	Edge new_edge = new Edge( super.get_vertex( p1 ), super.get_vertex( p2 ) );

		super.set_edge( "" + p1 + p2, new_edge );
		super.set_edge( "" + p2 + p1, new_edge );

		super.get_vertex(p1).neighbours.put( Integer.valueOf( p2 ), super.get_vertex( p2 ) );
		super.get_vertex(p2).neighbours.put( Integer.valueOf( p1 ), super.get_vertex( p1 ) );
	
		return new_edge;
    }
    
    public Edge delete_edge(int p1, int p2) {
    	Edge e = super.edges.get("" + p1 + p2);
    	if ( e != null ) {
    		super.delete_edge("" + p1 + p2);
    		super.get_vertex(p1).neighbours.remove(Integer.valueOf( p2 ));
    	}
    
		return e;
    }
    
	public Edge make_new_edge(int origin, int target) {
		Edge new_edge = new Edge( super.get_vertex( origin ), super.get_vertex( target ) );

		super.set_edge( new_edge );
		super.set_edge( origin, target, new_edge );
		super.get_vertex( origin ).neighbours.put( Integer.valueOf( target ), super.get_vertex( target ) );
		
		super.set_edge( target, origin, new_edge );
		super.get_vertex( target ).neighbours.put( Integer.valueOf( origin ), super.get_vertex( origin ) );
	
		return new_edge;
	}
	
	public Edge remove_edge(Edge e) {
		super.delete_edge(e);
		super.delete_edge(e.p1.getIndex(), e.p2.getIndex());
		super.delete_edge(e.p2.getIndex(), e.p1.getIndex());
		super.get_vertex(e.p1.getIndex()).neighbours.remove(Integer.valueOf( e.p2.getIndex() ));
		super.get_vertex(e.p2.getIndex()).neighbours.remove(Integer.valueOf( e.p1.getIndex() ));
		
		return e;
	}
	/*
	public SocNetSim2017.Edge remove_edge(int origin, int target) {
		SocNetSim2017.Edge e = super.get_edge(origin, target);
		super.delete_edge(e);
		//super.delete_edge(origin, target);
		//super.delete_edge(target, origin);
		super.get_vertex(origin).neighbours.remove(Integer.valueOf( target ));
		super.get_vertex(target).neighbours.remove(Integer.valueOf( origin ));
		
		return e;
	}
	*/

	public void setPajek_filename(String newPajekFilename) {
        this.pajek_filename = newPajekFilename;
    }
	public String getPajek_filename() { return this.pajek_filename;   }

	// this is called by SocNetSim2017.Network.java -> gen_edges()

	public void importPajekEdges(){
        System.out.println("NETWORK: importPajekVEs for totalVertices #" + super.size());

        try {
            BufferedReader inFile = new BufferedReader(new FileReader(getPajek_filename() ));
            System.out.println("Opening " + getPajek_filename() + " for edges...");

            // REDUNDANT ArrayList<SocNetSim2017.Vertex> edge_frequency = new ArrayList<SocNetSim2017.Vertex>();

			int count = vertices.size();

			String line = null;
			boolean startReading = false;
			while ((line = inFile.readLine()) != null) {
				if (line.equalsIgnoreCase("*Edges")) {
					startReading = true;
					continue;
				}

				if (startReading) {
					StringTokenizer st = new StringTokenizer(line);

                    // V origin = verticesMap.get(Integer.valueOf(st.nextToken()));
					// V target = verticesMap.get(Integer.valueOf(st.nextToken()));

                    // WARNING
                    // PAJEK nodes are 1-based, not 0-based; e.g. [ 1 "0-1-0-0-1" ]
                    // THEREFORE the largest node (token str) is totalAgents - 1
                    // e.g. an edge of [30 10] is actually internally [29 09]
                    int origin = Integer.valueOf(st.nextToken()) -1;
                    int target = Integer.valueOf(st.nextToken()) -1;

                    //System.out.println("DEBUG: Adding (0-based) origin " + origin + " -> target " + target);

					//if (!origin.equals(target) && !g.edgeExist(origin, target)) {
                    if ( origin != target && !super.exist( origin, target ) && !super.exist( target, origin ) ) {
                        this.make_new_edge(origin, target);
						//g.addEdge(origin, target);
					}
				}
			}
        }
        catch (IOException iox) {
            System.out.println("Problem opening " + getPajek_filename());
        }
        catch (NoSuchElementException e) {
            System.out.println("Error reading pajek file.");
        }

    }
}
