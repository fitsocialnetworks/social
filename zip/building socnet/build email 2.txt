From  	View message header detail Greg Paperin <gpaperin@infotech.monash.edu.au> 
Sent  	Monday, January 21, 2008 5:20 pm
To  	David Green <David.Green@infotech.monash.edu.au> , Sheree.Driver@infotech.monash.edu.au 
Cc  	 
Bcc  	 
Subject  	Building SocNetSim from command line
Attachments  	
Building SocNetSim from command line.zip	3.7MB


Hi David, Sheree,

According to David's request last week, I have created a step-by-step
manual of how to compile and build the social net sim from the command
line. I have specifically kept the manual very precise such that it
should be easy for everyone to follow it.

Attached is a ZIP archive that contains all necessary files and a text
document with the manual. If you have any questions or problems, please
let me know.

In any case, I do encourage you to use http://www.eclipse.org/ or any
other full-scale IDE (integrated development environment). It is
considerably more efficient in many respects. The time you'll spend for
configuration and learning will be made up within the first day of using
the IDE.

Cheers,

Greg))
